#ifndef _SOUND_H
#define _SOUND_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */


/****************************************************************** Typedefs */

/**
 * Defines a note that can be played by the Roomba.
 */
typedef struct {
	/**
	 * The pitch of the musical note, according to the MIDI note numbering scheme.
	 * Valid notes, that will be played by the Roomba are 31-127. Everything below 31
	 * is not played.
	 */
	uint8_t note;

	/**
	 * The duration of a musical note, in increments of 1/64th of a second.
	 * A half-second long musical note has a duration value of 32.
	 */
	uint16_t duration;
} note_t;

/**
 * Defines a song that can be played by the Roomba, using the function ::play_song().
 */
typedef struct {
	/**
	 * Beats per minute.
	 */
	uint16_t bpm;

	/**
	 * The time signature (::beats/4). This is usually 4.
	 */
	uint8_t beats;

	/**
	 * The number of notes defined in ::notes.
	 */
	uint16_t length;

	/**
	 * The notes for this song.
	 * According to the Roomba manual, a maximum of 16 notes can be played.
	 */
	note_t notes[];
} song_t;

/************************************************************** Global const */

extern const song_t empty_sound;
extern const song_t start_sound;
extern const song_t gimmick_pickup;
extern const song_t gimmick_pickup_failed;
extern const song_t use_gimmick_sound;
extern const song_t bombermansong;

/********************************************************** Global variables */


/************************************************ Global function prototypes */

void play_song(const song_t*);
void update_longsong();
inline uint8_t is_playing();

/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_SOUND_H */
