#ifndef _CALIBRATE_H
#define _CALIBRATE_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>
#include <menu-roomba.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

#define TRACK_MIN	(MIN_VAL(brrroomba.calibration.race_track_color))	//!< The minimum color value of the race track.
#define TRACK_MAX	(MAX_VAL(brrroomba.calibration.race_track_color))	//!< The maximum color value of the race track.
#define TRACK_AVG	(AVG(brrroomba.calibration.race_track_color))		//!< The average color value of the race track.
#define FLOOR_MIN	(MIN_VAL(brrroomba.calibration.out_of_track_color))	//!< The minimum color value of the floor.
#define FLOOR_MAX	(MAX_VAL(brrroomba.calibration.out_of_track_color))	//!< The maximum color value of the floor.
#define FLOOR_AVG	(AVG(brrroomba.calibration.out_of_track_color))		//!< The average color value of the floor.

/****************************************************************** Typedefs */

/**
 * Defines a color range with minimum and maximum value.
 * This is used for calibrated colors, as the values read from the sensors vary.
 */
typedef struct {
	/**
	 * The minimum value.
	 */
	uint16_t min_value;

	/**
	 * The maximum value.
	 */
	uint16_t max_value;
} color_range_t;

/**
 * Global calibration data.
 */
typedef struct {
	int32_t raw_angle;
	int32_t raw_distance;

	// Color data
	color_range_t race_track_color;
	color_range_t out_of_track_color;

	/**
	 * Buffer around each color.
	 */
	uint16_t buffer;

	/**
	 * Buffer in millimeter around each gimmick length.
	 */
	uint16_t gimmick_length_buffer;
} calibration_data_t;

/************************************************************** Global const */

/********************************************************** Global variables */

menu_t calibration_menu;

/************************************************ Global function prototypes */

int32_t get_angle(int32_t angle);
int32_t get_distance(int32_t distance);

/*************************************************** Global inline functions */


/******************************************************************** Macros */

/**
 * The average value for the given ::color_range_t.
 * @param color_range A value of type ::color_range_t.
 */
#define AVG(color_range)	(((color_range).min_value + (color_range).max_value) / 2)

/**
 * The minimum value for the given ::color_range_t with regard to the color buffer.
 */
#define MIN_VAL(color_range)	((color_range).min_value - brrroomba.calibration.buffer)

/**
 * The maximum value for the given ::color_range_t with regard to the color buffer.
 */
#define MAX_VAL(color_range)	((color_range).max_value + brrroomba.calibration.buffer)

/**
 * Test if the value is between (including) the values low and high. The two boundaries low and high may be given in reversed order.
 */
#define BETWEEN(value, low, high)	((low) < (high) ? ((low) <= (value) && (value) <= (high)) : ((high) <= (value) && (value) <= (low)))

/**
 * Calculates if the sensor is on the given ::color_range_t.
 * @param sensor The sensor to compare to the color range.
 * @param color_range The color to check.
 */
#define ON_COLOR(sensor, color_range)	(BETWEEN(sensor, MIN_VAL(color_range), MAX_VAL(color_range)))

/**
 * Test if the given sensor value is on the track.
 */
#define ON_TRACK(sensor)			(ON_COLOR(sensor, brrroomba.calibration.race_track_color))

/**
 * Test if the given sensor value is on the floor.
 */
#define ON_FLOOR(sensor)			(ON_COLOR(sensor, brrroomba.calibration.out_of_track_color))

/**
 * Test if the given sensor value is between the track and the floor.
 */
#define BETWEEN_TRACK_FLOOR(sensor)	(BETWEEN(sensor, FLOOR_AVG, TRACK_AVG))


#ifdef __cplusplus
}
#endif

#endif /* !_CALIBRATE_H */
