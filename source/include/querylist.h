#ifndef _QUERYLIST_H
#define _QUERYLIST_H 1

/****************************************************************** Includes */

#include <stdint.h>

#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */


/****************************************************************** Typedefs */


/************************************************************** Global const */


/********************************************************** Global variables */

extern volatile uint8_t query_input[];
extern volatile int32_t query_output[];

/************************************************ Global function prototypes */

void query_list(uint8_t num);
void query_list_plain(uint8_t num);
int32_t query_value(uint8_t packet_id);


/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_QUERYLIST_H */
