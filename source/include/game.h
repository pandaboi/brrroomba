#ifndef _GAME_H
#define _GAME_H 1

/****************************************************************** Includes */

#include <calibrate.h>
#include <sound.h>
#include <stdint.h>
#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

/****************************************************************** Typedefs */

/**
 * Defines a gimmick with all of its attributes.
 */
typedef struct {
	/**
	 * Unique id of the gimmick.
	 *
	 * Instead of the id, it would be possible to add two function pointers to this struct
	 * that are called, when the gimmick is activated and when it is deactivated.
	 * This would only be useful, if there are a lot of gimmicks.
	 */
	uint8_t id;

	/**
	 * The color of the gimmick.
	 */
	color_range_t color;

	/**
	 * The length of the gimmick in millimeter.
	 */
	uint16_t length;

	/**
	 * The sound to play, when the gimmick is picked up.
	 */
	const song_t *sound_pickup;

	/**
	 * The sound to play, when the gimmick is activated/used by the user.
	 */
	const song_t *sound_activate;

	/**
	 * The number of ticks, this gimmick will be active, after being activated.
	 * A Tick is one iteration of a while(true) loop.
	 */
	uint16_t duration;
} gimmick_t;

/**
 * Game data.
 */
typedef struct {
	int16_t velocity_max;	//!< the current maximum velocity. This will be changed with gimmicks.
	int16_t velocity;		//!< the current velocity
	int16_t radius;			//!< the current radius
	uint8_t bulletproof;	//!< boolean value, that indicates if the Roomba can get hit by red shells.
	
	/**
	 * The current gimmick, that the BrrRoomba has picked up.
	 * Once a gimmick is picked up, no other can be picked up until it is used.
	 * If no gimmick is available, this is ::NULL.
	 */
	gimmick_t* gimmick;
} game_data_t;

/************************************************************** Global const */


/********************************************************** Global variables */

extern gimmick_t gimmicks[];

/************************************************ Global function prototypes */

uint8_t game_main();
uint8_t manual_main();

/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_GAME_H */
