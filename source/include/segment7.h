#ifndef _SEGMENT7_H
#define _SEGMENT7_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */


/****************************************************************** Typedefs */


/************************************************************** Global const */


/********************************************************** Global variables */


/************************************************ Global function prototypes */

void update_7seg();

void set_7seg_ascii(const uint8_t* string, uint8_t length);
void set_7seg_string(const char* string);
void set_7seg_hex(int32_t val);
void set_7seg_dec(int32_t val);
void set_7seg_bin(int32_t val);

/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_SEGMENT7_H */
