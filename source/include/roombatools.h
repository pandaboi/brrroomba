#ifndef _ROOMBA_TOOLS_H
#define _ROOMBA_TOOLS_H

#define SENSOR_BUMPER		 7
#define SENSOR_BUTTONS		18
#define SENSOR_IR_OMNI		17
#define SENSOR_OVERCURRENTS	14
#define SENSOR_IR_LEFT		52
#define SENSOR_IR_RIGHT		53

#define LED_DEBRIS			0x01
#define LED_SPOT			0x02
#define LED_DOCK			0x04
#define LED_CHECK			0x08

#define BUTTON_CLEAN	0x01
#define BUTTON_SPOT		0x02
#define BUTTON_DOCK		0x04
#define BUTTON_MINUTE	0x08
#define BUTTON_HOUR		0x10
#define BUTTON_DAY		0x20

void leds(uint8_t,uint8_t,uint8_t);
void sleds(uint8_t,uint8_t);
void init_roomba(void);
void write_7seg(unsigned char,unsigned char,unsigned char,unsigned char);
char get_infrared(void);
void sseg(uint16_t val);
void drive_straight(int16_t vel);
void drive(int16_t vel, int16_t dir);
void stop();



#endif

