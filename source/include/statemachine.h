#ifndef _STATEMACHINE_H
#define _STATEMACHINE_H 1

/****************************************************************** Includes */

#include <stdint.h>

#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

/**
 * Special state, that is used to indicate, that the previous state should be activated.
 * This can be used, to exit from a state that can be activated from multiple other states.
 */
#define ST_PREVIOUS 0xff

/****************************************************************** Typedefs */

/**
 * Associates a state identifier with its function.
 */
typedef struct {
	/**
	 * The state, that must be active in order for this transition to be used.
	 */
	uint8_t state;

	/**
	 * The program logic of the state. Must return a valid state (or the special
	 * state ::ST_PREVIOUS) that will be activated next.
	 */
	uint8_t (*fn)();
} transition_t;


/************************************************************** Global const */


/********************************************************** Global variables */


/************************************************ Global function prototypes */

void sm_start(uint8_t start_state, uint8_t stop_state, transition_t* transitions, uint8_t count_transitions);

/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_STATEMACHINE_H */

