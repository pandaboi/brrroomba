#ifndef _MENU_ROOMBA_H
#define _MENU_ROOMBA_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

/**
 * The maximum length of a menu entry name.
 *
 * This must be defined, as a struct can only contain one variable length array.
 *
 * If this value is too small, a compiler warning is generated. This must be fixed under all circumstances!
 */
#define MAX_MENU_NAME 20


/**
 * Indicates, that the menu should continue as usual.
 */
#define MENU_CONTINUE	0x30

/**
 * Indicates, that the current submenu should be exited.
 */
#define MENU_BACK	0x10

/**
 * Indicates, that the whole menu should exit.
 */
#define MENU_EXIT	0x20

/****************************************************************** Typedefs */

struct menu;

/**
 * Definition of a menu entry.
 */
typedef struct {
	/**
	 * The name of the entry that will be displayed on the 7 segment display.
	 */
	char name[MAX_MENU_NAME];

	/**
	 * The function to call when the entry is selected.
	 * This function is always called, even if the entry has a sub menu defined.
	 * A value of NULL disables the function.
	 * The function should return one of ::MENU_CONTINUE, ::MENU_BACK, ::MENU_EXIT or any other custom code.
	 * If a custom code is returned, this is handled the same as ::MENU_EXIT, meaning the whole menu is exited.
	 */
	uint8_t (*fn)();

	/**
	 * An optional submenu, that will be displayed after executing the function fn.
	 */
	struct menu* submenu;
} menu_item_t;

/**
 * Definition of a menu. Contains several menu items.
 */
struct menu {
	/**
	 * The number of items stored in this menu.
	 */
	uint8_t size;

	/**
	 * The items for this menu.
	 */
	menu_item_t items[];
};
typedef struct menu menu_t;

/************************************************************** Global const */


/********************************************************** Global variables */


/************************************************ Global function prototypes */

uint8_t display_menu(const menu_t*, const uint8_t, const uint8_t);
uint8_t menu_back();
uint8_t menu_exit();

/*************************************************** Global inline functions */

/******************************************************************** Macros */

/**
 * Checks if the given code is handled by the menu.
 */
#define IS_MENU_CODE(code)	((code) == MENU_CONTINUE || (code) == MENU_BACK || (code) == MENU_EXIT)


#ifdef __cplusplus
}
#endif

#endif /* !_MENU_ROOMBA_H */
