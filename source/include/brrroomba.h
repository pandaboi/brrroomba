#ifndef _BRRROOMBA_H
#define _BRRROOMBA_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>

#include <calibrate.h>
#include <game.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

// Helper defines, to access buttons with reasonable names

#define BTN_OK				BTN_CLEAN		//!< Accept selection
#define BTN_UP				BTN_SPOT		//!< Select next
#define BTN_DOWN			BTN_DOCK		//!< Select previous
#define BTN_BACK			BTN_DAY			//!< Cancel

#define IR_DATA				(brrroomba.controller)

/**
 * Definition for all implemented BrrRoomba roles.
 */
enum roles_t {
	/// Roomba is controlled by the user using an IR remote control.
	ROLE_USER,

	/// Roomba is controlled by Chuck Norris.
	ROLE_CHUCK_NORRIS,

	/// Marker value, that contains the number of defined roles.
	ROLE_MAX,
};

/**
 * Some of the IR commands are mapped to button presses on the Roomba.
 * This means, that if a button is pressed on the IR remote,
 * the mapped button is marked as pressed even if it is not physically pressed on the Roomba.
 *
 * Thus checking the packet ::PACKET_BUTTONS does not guarantee, that the button is pressed on the Roomba.
 *
 * Using these mapped buttons should be avoided.
 */
#define IROBOT_580_TURNLEFT		129		//!< not mapped
#define IROBOT_580_FORWARD		130		//!< not mapped
#define IROBOT_580_TURNRIGHT	131		//!< not mapped
#define IROBOT_580_SPOT			132		//!< mapped to ::BTN_SPOT
#define IROBOT_580_MAX			133		//!< mapped to ::BTN_DOCK
#define IROBOT_580_SMALL		134		//!< mapped to ::BTN_CLEAN
#define IROBOT_580_MEDIUM		135		//!< mapped to ::BTN_CLEAN
#define IROBOT_580_LARGE		136		//!< mapped to ::BTN_CLEAN
#define IROBOT_580_PAUSE		137		//!< not mapped
#define IROBOT_580_POWERTOGGLE	138		//!< not mapped
#define IROBOT_580_LEFT			139		//!< not mapped
#define IROBOT_580_RIGHT		140		//!< not mapped
#define IROBOT_580_BACK			141		//!< not mapped

#define IROBOT_580_DOCK			143		//!< mapped to ::BTN_DOCK

/****************************************************************** Typedefs */


/**
 * Contains remote control commands.
 */
typedef struct{
    uint8_t up;
    uint8_t down;
    uint8_t left;
    uint8_t right;

    uint8_t stop;
    uint8_t ok;
} remote_controller_t;


/**
 * Contains all data that is used by the BrrRoomba application.
 */
typedef struct {
	/**
	 * The role of the Roomba for the game. Roles define different behaviors during the game.
	 */
	uint8_t role;

	/**
	 * The calibration data.
	 */
	volatile calibration_data_t calibration;

	/**
	 * The game data.
	 */
	volatile game_data_t game;

	/**
	 * The remote controller data.
	 */
	remote_controller_t controller;
} brrroomba_t;

/************************************************************** Global const */

extern const remote_controller_t remote_controllers[2];

extern const remote_controller_t no_remote;

/********************************************************** Global variables */

/**
 * The BrrRoomba definition.
 */
brrroomba_t brrroomba;

/************************************************ Global function prototypes */

/*************************************************** Global inline functions */

/******************************************************************** Macros */

/**
 * Checks if the given buttons or the given ir signal contain the OK command.
 * @param btns The received value for the packet ::PACKET_BUTTONS.
 * @param ir The received value for one of the IR sensors.
 */
#define IS_OK(btns, ir)	(BTN_OK & btns || (ir) == IR_DATA.ok || (ir) == IR_DATA.right)
#define IS_UP(btns, ir)	(BTN_UP & btns || (ir) == IR_DATA.up)
#define IS_DOWN(btns, ir)	(BTN_DOWN & btns || (ir) == IR_DATA.down)
#define IS_BACK(btns, ir)	(BTN_BACK & btns || (ir) == IR_DATA.stop || (ir) == IR_DATA.left)

#define ROOMBA_RELEASE_BACK() {	\
	ROOMBA_BUTTON_RELEASE(BTN_BACK);	\
	ROOMBA_IR_RELEASE(IR_DATA.stop);	\
	ROOMBA_IR_RELEASE(IR_DATA.left);	\
}

#define ROOMBA_RELEASE_OK() {	\
	ROOMBA_BUTTON_RELEASE(BTN_OK);	\
	ROOMBA_IR_RELEASE(IR_DATA.ok);	\
	ROOMBA_IR_RELEASE(IR_DATA.right);	\
}

#define ROOMBA_RELEASE_UP() {	\
	ROOMBA_BUTTON_RELEASE(BTN_UP);	\
	ROOMBA_IR_RELEASE(IR_DATA.up);	\
}

#define ROOMBA_RELEASE_DOWN() {	\
	ROOMBA_BUTTON_RELEASE(BTN_DOWN);	\
	ROOMBA_IR_RELEASE(IR_DATA.down);	\
}

#define ROOMBA_WAIT_OK() {	\
	while (true) {	\
		if (IS_OK(query_value(PACKET_BUTTONS), query_value(PACKET_IR_OMNI))) {	\
			ROOMBA_RELEASE_OK();	\
			break;	\
		}	\
	}	\
}


#ifdef __cplusplus
}
#endif

#endif /* !_BRRROOMBA_H */
