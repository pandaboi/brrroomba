
#ifndef _CHUCK_NORRIS_H
#define _CHUCK_NORRIS_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>
#include <menu-roomba.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

/****************************************************************** Typedefs */

/************************************************************** Global const */

/********************************************************** Global variables */

/************************************************ Global function prototypes */

void chuck_norris_main();

/*************************************************** Global inline functions */


/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_CHUCK_NORRIS_H */
