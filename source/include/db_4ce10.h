/**
 * $Id: db_4ce10.h 478 2011-04-19 08:06:54Z bradatch $
 */

/******************************************************************************

File: db_4ce10.h

Project: Roomba Embedded Systems Training

Description: ALTERA 4CE10 on-board devices using OR32
             The addresses in this file can also be used for assembly code.
             If you include this file into your assemly file, make
             sure that you have __ASSEMBLY__ defined.

Author: Florian Kluge <kluge@informatik.uni-augsburg.de>
        Universität Augsburg

Created: 22.02.2011

*******************************************************************************

Modification history:
---------------------
22.02.2011 (FAK) Created from de2-70.h


*/

#ifndef BOARD_INCLUDE
#error Do not directly include this file, use <board.h> instead!
#endif

#ifndef _DB_4CE10_H
#define _DB_4CE10_H 1


/****************************************************************** Includes */

#ifndef __ASSEMBLY__
#include <stdint.h>
#endif /* !__ASSEMBLY__ */

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

//////////////
// IR outputs
//////////////

// addresses
#define A_IR_SENDER_BASE		0xf00000b0
#define A_IR_SENDER_CTRL		(A_IR_SENDER_BASE + 0x00)
#define A_IR_SENDER_DATA		(A_IR_SENDER_BASE + 0x04)

#define HAVE_IR 1

#ifndef __ASSEMBLY__
  // devices
#define IR_SENDER_CTRL			(*((uint32_t volatile *) A_IR_SENDER_CTRL))
#define IR_SENDER_DATA			(*((uint32_t volatile *) A_IR_SENDER_DATA))
#endif /* !__ASSEMBLY__ */


//////////////
// LED outputs
//////////////

// addresses
#define A_LED         0xf00000c0 /*!< LED address */
  
#ifndef __ASSEMBLY__
  // devices
  /*! On-Board LEDs; check #LED_MASK */
#define LED (*((uint32_t volatile *) A_LED))
#endif /* !__ASSEMBLY__ */

#define LED_MASK 0x3f000000
  

  //////////
  // Buttons
  //////////

#define HAVE_BUTTONS 1

  // adresses
#define A_PIO_BT_BASE     0xf00000e0 /*!< Two buttons base address */
#define A_PIO_BT_DATA     (A_PIO_BT_BASE + 0x0) /*!< Data */
#define A_PIO_BT_DIR      (A_PIO_BT_BASE + 0x4) /*!< Direction */
#define A_PIO_BT_IRQ_MASK (A_PIO_BT_BASE + 0x8) /*!< IRQ mask */
#define A_PIO_BT_EC       (A_PIO_BT_BASE + 0xc) /*!< Edge Capture */

#ifndef __ASSEMBLY__
  // devices
#define PIO_BT_BASE      (*((uint8_t volatile *) A_PIO_BT_BASE))
#define PIO_BT_DATA      (*((uint8_t volatile *) A_PIO_BT_DATA)) /*!< Four buttons */
#define PIO_BT_DIR       (*((uint8_t volatile *) A_PIO_BT_DIR)) /*!< Buttons IRQ maske */
#define PIO_BT_IRQ_MASK  (*((uint8_t volatile *) A_PIO_BT_IRQ_MASK)) /*!< Buttons IRQ maske */
#define PIO_BT_EC        (*((uint8_t volatile *) A_PIO_BT_EC)) /*!< Buttons IRQ maske */
#endif /* !__ASSEMBLY__ */


  ///////
  // UART
  ///////

#define HAVE_UART 1

  // addresses
#define A_UART_BASE   0xf0000000 /*!< UART base address */
#define A_UART_RX     (A_UART_BASE + 0x00) /*!< UART rxdata */
#define A_UART_TX     (A_UART_BASE + 0x04) /*!< UART txdata */
#define A_UART_ST     (A_UART_BASE + 0x08) /*!< UART status */
#define A_UART_CT     (A_UART_BASE + 0x0c) /*!< UART control */
#define A_UART_DV     (A_UART_BASE + 0x10) /*!< UART divisor */
#define A_UART_EP     (A_UART_BASE + 0x14) /*!< UART end-of-packet */

#ifndef __ASSEMBLY__
  // devices
#define UART_BASE (*((uint32_t volatile *) A_UART_BASE ))
#define UART_RX   (*((uint8_t volatile *)  A_UART_RX   )) /*!< UART rxdata */
#define UART_TX   (*((uint8_t volatile *)  A_UART_TX   )) /*!< UART txdata */
#define UART_ST   (*((uint32_t volatile *) A_UART_ST   )) /*!< UART status */
#define UART_CT   (*((uint32_t volatile *) A_UART_CT   )) /*!< UART control */
#define UART_DV   (*((uint32_t volatile *) A_UART_DV   )) /*!< UART divisor */
#define UART_EP   (*((uint32_t volatile *) A_UART_EP   )) /*!< UART end-of-packet */
#endif /* !__ASSEMBLY__ */



  ////////////////////////
  // JTAG Character Device
  ////////////////////////

  // addresses
#define A_JTAG_BASE 0xf0000020 /*!< JTAG CD base address */
#define A_JTAG_DR   (A_JTAG_BASE + 0x00) /*!< JTAG CD data register */
#define A_JTAG_CR   (A_JTAG_BASE + 0x04) /*!< JTAG CD control registers */
#define A_JTAG_TX   (A_JTAG_DR   + 0x00) /*!< JTAG send data */

#ifndef __ASSEMBLY__
  // devices
#define JTAG_BASE (*((uint32_t volatile *) A_JTAG_BASE )) /*!< JTAG CD base address */
#define JTAG_DR   (*((uint32_t volatile *) A_JTAG_DR ))   /*!< JTAG CD data register */
#define JTAG_CR   (*((uint32_t volatile *) A_JTAG_CR ))   /*!< JTAG CD control registers */
#define JTAG_TX   (*((uint8_t  volatile *) A_JTAG_TX ))   /*!< UART txdata */
#endif /* !__ASSEMBLY__ */






  //////////
  // Mermory
  //////////

  // addresses
#define A_SDRAM         0x00000000 /*!< on-board SDRAM */
#define A_OCRAM         0xff000000 /*!< on-chip RAM - here reside the execption vectors */

#ifndef __ASSEMBLY__
  // devices
#define SDRAM (*((uint32_t volatile *) A_SDRAM )) /*!< on-board SDRAM */
#define OCRAM (*((uint32_t volatile *) A_OCRAM )) /*!< on-chip RAM - here reside the execption vectors */
#endif /* !__ASSEMBLY__ */



  //////
  // ???
  //////

  // addresses
#ifndef __ASSEMBLY__
  // devices
#endif /* !__ASSEMBLY__ */


  ///////
  // IRQs
  ///////

#define N_IRQS     20 /*!< Number of IRQs available */

#define IRQ_UART    2 /*!< IRQ from UART */
#define IRQ_BUTTON  3 /*!< IRQ from push-button */


/****************************************************************** Typedefs */


/************************************************************** Global const */


/********************************************************** Global variables */


/************************************************ Global function prototypes */


/*************************************************** Global inline functions */


/******************************************************************** Macros */

	/*
name:	just a name for the section
addr:	4-bit part of the exception adress, use (entry & 0xff00)>>8
	*/
#define UNHANDLED_EXCEPTION(name, addr)\
	.global name ## _vec		   ;\
name ## _vec:				   ;\
	LOAD_SYMBOL_2_GPR(r5, A_LED) ;\
	l.addi	r6, r0, lo(addr)	;\
	l.slli	r6, r6, 16		;\
	l.sw    0x0(r5), r6	    ;\
	l.j	0		           ;\
	l.nop


#ifdef __cplusplus
}
#endif

#endif /* !_DB_4CE10_H */
