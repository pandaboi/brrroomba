#ifndef _SETTINGS_H
#define _SETTINGS_H 1

/****************************************************************** Includes */

#include <stdint.h>
#include <board.h>
#include <brrroomba.h>
#include <menu-roomba.h>
#include <tools.h>

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************* Defines */

/****************************************************************** Typedefs */

/************************************************************** Global const */

/********************************************************** Global variables */

menu_t settings_menu;

/************************************************ Global function prototypes */

int32_t change_value(int32_t, int8_t, int32_t, int32_t, uint8_t, void (*display_fn)(int32_t));
uint8_t load_defaults();

/*************************************************** Global inline functions */

/******************************************************************** Macros */


#ifdef __cplusplus
}
#endif

#endif /* !_SETTINGS_H */
