#!/bin/bash

echo "***************************************************************"
echo "* Transfers file given by first argument to usbblaster device *"
echo "***************************************************************"
echo ""


file=$1
read -p "Be sure that the device is ready (buttons P3+P4) - Press 'Enter' to start transfering \"$file\" ..."


if [ ! $file  ];
then
	echo "Provide a filename; Usage: './transfer.sh main.bin'"
else

	if [ -f $file ];
	then
		echo "Transferring \"$1\" over usbblaster"
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/altera11.0/quartus/linux/
		cat $1 | /opt/altera11.0/nios2eds/bin/nios2-terminal --cable 2
	else
		echo "File does not exist; Usage: './transfer.sh main.bin'"
	fi
fi
