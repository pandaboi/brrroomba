Game {#game}
====

[TOC]

Start     {#game-start}
-----

Starts a new game depending on the selected [role](@ref settings-role).
The following game modes are available.

- \ref user-game
- \ref chuck-norris

Manual    {#game-manual}
------

This menu entry is used to allow easy manual driving without having to modify any settings or starting a special gaming mode.
The Roombas role will be forced to *User*.
This is essentially the same, as when using the Role *User* and selecting [start](@ref game-start).
The only difference is the first state that will be activated (@ref ST_START vs @ref ST_MANUAL_DRIVE).
