Chuck Norris {#chuck-norris}
============

> *There are no words to describe Chuck Norris.*

The only thing known to us is following:

Chuck Norris is moving inside the racetrack and tries to hinder other players, who are not Chuck Norris.
As Chuck Norris knows the BrrRoomba game is awesome, he regularly checks his front bumpers and turns around if he hit something.
Otherwise, there would be no more BrrRoomba.

For undetermined reasons, Chuck Norris regularly breaks out of the racetrack and follows a path unknown to anyone else.

Chuck Norris also has the task to play the background music.
The *Up* Button can be used to start and stop the music.
Stopping does not work immediately.
It has a maximum delay of 16 notes (see @ref MAX_LENGTH).

Command        		|Button                   |IR Button
------------------------|-------------------------|---------
Toggle Music Playback   |[Spot](@ref BTN_SPOT)    |[Up](@ref remote_controller_t.up)

