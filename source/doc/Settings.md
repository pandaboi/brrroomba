Settings {#settings}
========

[TOC]

When changing values, the *Back* button can be used to cancel the edit mode without saving.

IR Remote {#settings-ir-remote}
---------

This lets the user select an IR remote control for the Roomba.
To select an IR remote, point it to the Roomba and press *OK* on the remote.
If the Roomba correctly identified the remote, it will display the remotes ID on the seven segment display and return to the menu once the *Ok* button is released.

To disable the IR remote, press the *OK* button on the Roomba.


Role {#settings-role}
----

Set the role of the Roomba in the game.
Following roles can be selected:

- [User](@ref user-game)
  Normal game mode. The Roomba is user controlled.
- [Chuck Norris](@ref chuck-norris)
  Autonomous Roomba, that is not confined to the track.
  The Chuck Norris is driving randomly inside the track and tries to hinder other players.


IR Test {#settings-ir-test}
-------

Simple test code for the IR remote control.
Press any buttons on the IR remote and the mapped command will be displayed on the Roombas display.

This mode can be exited only with physical buttons.


Button Test {#settings-button-test}
-----------

Simple test code for the Roombas physical buttons.
Press any buttons on the Roomba and the mapped command will be displayed on the its display.
This is used to check, that no IR mapped buttons are received as physical button presses on the Roomba.

This mode can be exited only by pressing the Roombas bumper or lifting the Roomba off the ground.
The IR can not be used to leave this mode, as the IR buttons may be mapped to physical buttons.


Load Defaults {#settings-load-defaults}
-------------

Loads several default values for settings and calibrations.
These are the same values, that are set when the application starts.

See @ref load_defaults() for more info on what values will be loaded.
