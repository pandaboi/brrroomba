Calibration {#calibration}
===========

[TOC]

When changing values, the *Back* button can be used to cancel the edit mode without saving.

Angle {#calibration-angle}
-----

Calibrates the angle retrieved from the Roomba.
Without calibration, the values returned to the packet @ref PACKET_ANGLE are not specified in degree.

The calibration is performed automatically using a marker with the race track color on the floor.

- Put a marker with the race track color on the floor.
- Place the Roomba above the marker.
  The Roomba must see the marker only once when turning 360 degree.
  The front left cliff sensor should not be above the marker.
  Instead it should be a few centimeter right of it.
- Start the calibration.
  The Roomba will start turning until it sees the marker for the first time.
  Then it will stop for a short period and start turning again.
  Once it sees the marker for a second time, the Roomba knows it just performed a turn of exactly 360 degree.
  The calibrated value will be displayed on the Roomba.
- Press *Ok* to leave the angle calibration.

Be sure to calibrate the [race track color](@ref calibration-colors-racetrack) first.
The calibration routine can not be aborted once started.

Distance {#calibration-distance}
--------

Calibrates the driven distance retrieved from the Roomba.
Without calibration, the values returned to the packet @ref PACKET_DISTANCE are not specified in millimeters.

The calibration is performed automatically using two markers with the race track color on the floor.

- Put two markers with the race track color on the floor.
  The distance between the markers should be exactly 1 meter.
- Place the Roomba in front of the first marker.
  The front left cliff sensor should not be above the marker.
- Start the calibration.
  The Roomba will start driving straight until it sees the first marker.
  Then it will stop for a short period and start driving again.
  Once it sees the second marker, the Roomba knows it just drove exactly 1 meter.
  The calibrated value will be displayed on the Roomba.
- Press *Ok* to leave the distance calibration.

Be sure to calibrate the [race track color](@ref calibration-colors-racetrack) first.
The calibration routine can not be aborted once started.

Colors {#calibration-colors}
------

Colors are calibrated all in the same way.
The front left cliff sensor is used to read the color values.
The values will be displayed on the Roomba in real time.

Once the values seem to be correct, start pressing *Up*.
While *Up* is pressed, the minimum and maximum value read from the sensor will be saved.
This can be done multiple times.
To get the best results, try moving the Roomba, while holding the *Up* button.
The Roomba drives straight, when pressing the *Down* button.

To finish the calibration, press *Ok*.
The minimum value will be displayed when pressing *Down*.
The maximum value, when pressing *Up*.
To return to the menu, press *Ok*.

### Race Track ### {#calibration-colors-racetrack}

Calibration of the race track color.
See @ref calibration-colors.

### Floor ### {#calibration-colors-floor}

Calibration of the floor color.
See @ref calibration-colors.

### Buffer ### {#calibration-colors-buffer}

Sets an additional buffer around the calibrated colors.
This value should be around 50.
The values read from the cliff sensor, vary depending on the current illumination and the current driving speed of the Roomba.
Thus the minimum and maximum color values should not be fixed but account for a bit of variance.

Gimmicks {#calibration-gimmicks}
--------

### C-Acceleration Lane ### {#calibration-gimmicks-caccel}

Calibration of the color for the [acceleration lane](@ref gimmick-acceleration-lane) gimmick.
See @ref calibration-colors.

### L-Acceleration Lane ### {#calibration-gimmicks-laccel}

Sets the length of the acceleration lane in millimeter.

### C-Red Shell ### {#calibration-gimmicks-credshell}

Calibration of the color for the [red shell](@ref gimmick-redshell) gimmick.
See @ref calibration-colors.

### L-Red Shell ### {#calibration-gimmicks-lredshell}

Sets the length of the red shell in millimeter.

### Buffer ### {#calibration-gimmicks-buffer}

Sets an additional buffer around the gimmick lengths in millimeter.

This buffer is required, as the distance read when detecting a gimmick will never be exactly the specified value.
Depending on the Roombas speed, the current battery level and maybe some other factors, the read distance will vary to some degree.
