Menu {#menu}
====

[TOC]

Once the BrrRoomba application is started, the main menu will be active.
This menu is used to configure or test the application and to start a new game.

The menu can be navigated using the default [controls](@ref control).
*OK* will enter a submenu, while *Back* will leave a submenu and return to its parent.

The weekday LEDs on the Roomba indicate the current menu level.
With each entered submenu, this level increases.
Leaving a submenu decreases the level.

Main Menu {#main-menu}
---------

The menu contains the following items:

- [1 Game](@ref game)
  - [1 Start](@ref game-start)
    Starts a new game depending on the selected role.
  - [2 Manual](@ref game-manual)
    Starts a new game with the role *USER* and initial state set to @ref ST_MANUAL_DRIVE.
- [2 Settings](@ref settings)
  Configures the BrrRoomba application.
  - [1 IR Remote](@ref settings-ir-remote)
    Select an IR remote control for this Roomba.
  - [2 Role](@ref settings-role)
    Sets the role of the Roomba.
    Depending on the role, the Roomba has a different function in the game.
  - [3 IR Test](@ref settings-ir-test)
    Test to see if buttons from a IR remote control are read correctly.
  - [4 Button Test](@ref settings-button-test)
    Test to see if buttons on the Roomba are read correctly.
  - [5 Load Defaults](@ref settings-load-defaults)
    Load several default settings.
- [3 Calibration](@ref calibration)
  Calibrates the Roomba's sensors.
  - [1 Angle](@ref calibration-angle)
    Calibrates the angle to allow exact turns.
  - [2 Distance](@ref calibration-distance)
    Calibrates the distance as it is read from the Roomba.
  - [3 Colors](@ref calibration-colors)
    Calibrate the floor and racetrack colors.
    - [1 Race Track](@ref calibration-colors-racetrack)
      Calibrates the color for the racetrack.
    - [2 Floor](@ref calibration-colors-floor)
      Calibrates the color for the floor.
    - [3 Buffer](@ref calibration-colors-buffer)
      Sets the buffer to use for color detection.
  - [4 Gimmicks](@ref calibration-gimmicks)
    Calibrate the colors and lengths used for the gimmicks.
    - [1 C-Acceleration Lane](@ref calibration-gimmicks-caccel)
      Calibrates the color for the acceleration lane gimmick.
    - [2 L-Acceleration Lane](@ref calibration-gimmicks-laccel)
      Calibrates the length for the acceleration lane gimmick.
    - [3 C-Red Shell](@ref calibration-gimmicks-credshell)
      Calibrates the color for the red shell gimmick.
    - [4 L-Red Shell](@ref calibration-gimmicks-lredshell)
      Calibrates the length for the red shell gimmick.
    - [5 Buffer](@ref calibration-gimmicks-buffer)
      Sets the buffer to use for gimmick lengths detection.
- [4 Exit](@ref exit)
  Power off the Roomba.

The following sections describe these menu items and their function in more detail.

Exit        {#exit}
----

This will power off the Roomba by issuing a @ref CMD_POWER command.
The application will continue running, as the board will still get power from the Roomba.

After this item is selected, the BrrRoomba will jump to the [init state](@ref running).
The BrrRoomba can then be started using the buttons on the Board.
Do not forget to turn the Roomba on before restarting the application.
