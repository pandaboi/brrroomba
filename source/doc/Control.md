Control {#control}
=======

The BrrRoomba uses a simple and intuitive system to control the application.
There are only a few commands required.
One physical button on the Roomba and one or two buttons on the IR remote control are mapped to each command.

These commands will be used in the menu and for nearly all configurations.
During the game, the Roomba and IR buttons are mapped differently.

Command       |Button                   |IR Button
--------------|-------------------------|---------
Up            |[Spot](@ref BTN_SPOT)    |[Up](@ref remote_controller_t.up)
Down          |[Dock](@ref BTN_DOCK)    |[Down](@ref remote_controller_t.down)
OK            |[Clean](@ref BTN_CLEAN)  |[Ok](@ref remote_controller_t.ok)/[Right](@ref remote_controller_t.right)
Cancel/Back   |[Day](@ref BTN_DAY)      |[Exit](@ref remote_controller_t.stop)/[Left](@ref remote_controller_t.left)

Whenever necessary the meaning of these commands will be specified in more detail.
