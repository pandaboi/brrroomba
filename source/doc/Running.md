Running {#running}
=======

To get the BrrRoomba application running, the program must first be build and then flashed onto the board.

~~~~~
cd source/
make
sh transfer.sh main.bin
~~~~~

The BrrRoomba application must be flashed twice, if one of the following is true:
- The Roomba was previously off
- The Roomba was reset

Once the BrrRoomba is successfully flashed onto the board, it will wait to initialize the Roomba.
- Press **KEY3** to get the Roomba into Full mode. The Roombas blue (dirt detect) LED will turn on.
- After a second press **KEY4** to enter the [main menu](@ref menu).

There is a restart mechanism for the situation when **KEY3** was pressed before the Roomba was turned on:
Press **KEY4** and press it again immediately after releasing it (the time the button is released must not exceed 100 ms).
The initialization routine will then start from the beginning.
