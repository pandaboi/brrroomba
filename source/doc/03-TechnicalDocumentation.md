Technical Documentation {#technical-documentation}
=======================

The user controlled game mode (see @ref user-game) can be described with the following state chart.
This is a simplified state chart, that does not use the official syntax consistently.
Nonetheless, the source code can be understood better with this state chart.

![State Chart](STATE_CHART.svg)
\image latex STATE_CHART.eps "State Chart" width=\textwidth
