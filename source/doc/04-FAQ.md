FAQ {#faq}
===

* *HELP! My Roomba software crashed!*

  Maybe you requested an invalid packet id, dude!

* *HELP! My Roomba cannot be resetted!*
  
  Maybe you tried to load a song to a song-id, that is currently playing.
  This may crash your Roomba, with the only reset-option to take out the battery.

Embedded Programming Guidelines {#faq-guidelines}
-------------------------------

* Always use `volatile`!
* Never redefine variable names in different scopes (excludes `static` variables).
* Use a maximum compiler optimization of *O1* or *O0*. *O2* may only be used, if the source code is top notch.
* If the Roomba returns wrong values, just wait a minute or so.
* The BrrRoomba application does not work after the Roomba was first started. Just flash the application a second time and DO NOT reset the Roomba.
