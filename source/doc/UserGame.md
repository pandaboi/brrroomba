User Game {#user-game}
=========

[TOC]

This is the default, user controlled game mode.


## Controls ## {#user-game-controls}

Following controls are used throughout the game:
 
Command        |Button                   |IR Button
---------------|-------------------------|---------
Increase Speed |                         |[Up](@ref remote_controller_t.up)
Decrease Speed |                         |[Down](@ref remote_controller_t.down)
Turn Left      |                         |[Left](@ref remote_controller_t.left)
Turn Right     |                         |[Right](@ref remote_controller_t.right)
@ref pause-menu|[Clean](@ref BTN_CLEAN)/[Day](@ref BTN_DAY)|[Exit](@ref remote_controller_t.stop)

The [Ok](@ref remote_controller_t.ok) button on the IR remote is mapped differently depending on whether the Roomba is currently [following the racetrack](@ref game-game) or [manually controlled](@ref manual-drive) by the user.

To let the Roomba drive, it is not required to hold the *Up* button continuously.
After releasing the buttons, the Roomba will continue driving with the set speed.
This is done in order to send as few IR signals as possible.
When sending IR signals from multiple remotes at the same time, the signals will overlap and cause the Roombas to receive nothing.

Turns behave differently, as these are only done, as long as the [Left](@ref remote_controller_t.left)/[Right](@ref remote_controller_t.right) buttons are pressed.
Once these buttons are released, the turn radius will be reset to its previous value.

The speed is increased/decreased using a simple but effective algorithm.
There is a maximum speed defined (@ref game_data_t.velocity_max).
Until this maximum (or its negative counterpart) is reached, the speed change is following:

- Increase: \f$1 + ((velocity_{max} - velocity_{current}) / 64)\f$
- Decrease: \f$1 + ((velocity_{max} + velocity_{current}) / 64)\f$

Thus the Roomba has a higher acceleration when it is driving slow and a higher decceleration when it is driving fast.

## Pause Menu ## {#pause-menu}

The pause menu contains the following items:

- **1 Resume** Return to the game.
- **2 Manual** Go to the [manual driving](@ref manual-drive) mode.
- **3 Exit** Quit the game and return to the [main menu](@ref main-menu).

The pause menu can be left either by choosing **1 Resume** or by using the *Back* button.

When entering the pause menu, the Roomba is stopped.
After leaving the menu, the Roomba's speed will *not* be reset to its previous value.
This is to ensure, that nothing unexpected happens, after being paused for to long.
The Roomba will, however resume with the previously active state.

## Game ## {#game-game}

Command         |IR Button
----------------|---------
Apply Gimmick   |[Ok](@ref remote_controller_t.ok)

Playing the game does not require any special knowledge.
There are only a few points to remember:

- Gimmicks will be picked up with the outer left cliff sensor
- Do not take turns to fast, otherwise you may miss the turn
- Do not crash into stuff. This will get you banana'ed.
- Do not try to manually steer using *Left* / *Right* buttons in turns

Whenever the Roomba's bumper are activated or it was shot by another Roomba, it will perform a 360° rotation.
Internally this is called the *banana state*, as it is modeled after the effect of a banana in Mario Kart.

To better position the Roomba for gimmick pickups, turns or branches in the track, the Roomba can be set to drive with the front *left* or *right* cliff sensor above the track.
Press *Left* once to drive with the front right cliff sensor.
The left cliff sensor used to pickup gimmicks, will now be farther away from the track.
Using the *Right* button for the other side works equivalent.


## Manual drive ## {#manual-drive}

Command         |IR Button
----------------|---------
Follow racetrack|[Ok](@ref remote_controller_t.ok)

While driving manually, no gimmicks can be applied or picked up.
However, the Roomba can still be on the bad side of the @ref gimmick-redshell.

The [Left](@ref remote_controller_t.left) and [Right](@ref remote_controller_t.right) buttons behave differently depending on the current speed.
If the Roomba is driving very slow (i.e. nearly standing still), turning left or right will map to turns in place.
Otherwise the Roomba will turn with a radius of 150 mm.

If the bumper was pressed for more than about a second, the Roomba will stop.
It will not stop immediately, as it is quite easy to move along a wall and accidentally get the bumper activated.


# Gimmicks # {#gimmicks}

Only one gimmick can be carried by each Roomba at any given point in time.
If a Roomba already has a gimmick it cannot pickup another one, until the current gimmick is applied.

Each of the following will result in a different sound (possibly also different depending on the gimmick type):
- Gimmick picked up
- Gimmick applied
- Gimmick un-applied

## Acceleration Lane ## {#gimmick-acceleration-lane}

![Bullet Bill as seen in Mario Kart](bulletbill.png)
\image latex bulletbill.png "Bullet Bill as seen in Mario Kart" width=0.2\textwidth

The acceleration lane is the gimmick with the most visible effect.
If applied, the maximum speed will be increased from the default value of 300 to unlimited.
The unlimited speed will have effect for about 40 seconds.

Do not forget to slow down in turns, otherwise you may fall off the track.


## Red Shell ## {#gimmick-redshell}

![Red Shell as seen in Mario Kart](redshell.png)
\image latex redshell.png "Red Shell as seen in Mario Kart" width=0.2\textwidth

The *Red Shell* is a gimmick, that can be picked up and used to shoot other Roombas.
The red shell will be fired for about one second using the IR sender mounted on top of each Roomba.
While shooting, the current Roomba is bulletproof with regard to other Roombas shooting him.

The Red Shell has a maximum distance for about 1 to 2 meters.
To hit another Roomba, do not forget to actually aim for him!
