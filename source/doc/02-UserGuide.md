User Guide {#user-guide}
==========

This manual is divided in the following sections:
- \subpage running
- \subpage control
- \subpage menu
  - \subpage game
  - \subpage settings
  - \subpage calibration
- \subpage user-game
- \subpage chuck-norris
