/**
 * Song MIDIs:
 * http://moviethemes.net/starwars.html
 * http://www.angelfire.com/ak/skatermidi/swars.html
 */

/****************************************************************** Includes */

#include <sound.h>
#include <querylist.h>
#include <board.h>
#include <led.h>
#include <pio.h>
#include <roomba.h>
#include <tools.h>
#include <uart.h>

/******************************************************************* Defines */

/**
 * The maximum number of songs, that can be stored on the Roomba.
 */
#define MAX_SONGS	4

/**
 * The maximum number of notes per song. This is a limit for songs on the Roomba.
 * This application avoids this limit with the special functions ::play_longsong() and ::update_longsong().
 */
#define MAX_LENGTH	16

//	NOTES # => +1, all over 8 octaves (12 Notes)
// 	Example:
//	NOTE_G# = NOTE_G + 1
//	NOTE_G7 = NOTE_G + 7 * OCTAVE

#define OCTAVE	12
#define NOTE_G 	31
#define NOTE_A 	33
#define NOTE_H 	35
#define NOTE_B 	NOTE_H
#define NOTE_C	36
#define NOTE_D	38
#define NOTE_E	40
#define NOTE_F	41

#define PAUSE 	30

// NOTE LENGTHs  1/64th of a second
#define FULL_NOTE		16
#define HALF_NOTE		8
#define	QUARTER_NOTE	4
#define EIGHTH_NOTE		2
#define SIXTEENTH_NOTE	1

/******************************************************* Function prototypes */

static void play_longsong(const song_t *song);
static void continue_load_longsong(uint8_t);
static void play_sound(uint8_t);
static inline uint16_t calculate_seconds(uint8_t duration, const song_t *song);

/************************************************************** Global const */

const song_t empty_sound = {
		120,
		4,
		1,
		{
			{ PAUSE	, HALF_NOTE		},
		}
};

const song_t gimmick_pickup = {
		240,
		4,
		2,
		{
				{ NOTE_F +	3*OCTAVE, HALF_NOTE	},
				{ NOTE_F +	4*OCTAVE, FULL_NOTE	},
		}
};

const song_t gimmick_pickup_failed = {
		240,
		4,
		3,
		{
				{ NOTE_F +	3*OCTAVE, HALF_NOTE	},
				{ NOTE_C +	3*OCTAVE, HALF_NOTE	},
				{ NOTE_F +	2*OCTAVE, FULL_NOTE	},
		}
};

const song_t start_sound = {
	480,
	4,
	5,
	{
		{ NOTE_A +		4*OCTAVE, FULL_NOTE	},
		{ PAUSE					, HALF_NOTE		},
		{ NOTE_A +		4*OCTAVE, FULL_NOTE	},
		{ PAUSE					, HALF_NOTE		},
		{ NOTE_A +		5*OCTAVE, FULL_NOTE	},
	}
};

const song_t use_gimmick_sound = {
		480,
		4,
		1,
		{
			{ NOTE_A +	5*OCTAVE, FULL_NOTE	},
		}
};

const song_t bombermansong = {
		705*7,
		4,
		197,
		{
		{ NOTE_B+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_A+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_A+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	3*OCTAVE, 64	},
		{ PAUSE		, 128	},
		{ NOTE_F+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 128	},
		{ NOTE_A+	2*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	2*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_A+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_F+1+	5*OCTAVE, 256	},
		{ PAUSE		, 128	},
		{ NOTE_E+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 64	},
		{ PAUSE		, 224	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 256	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 128	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_A+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_F+1+	5*OCTAVE, 256	},
		{ PAUSE		, 128	},
		{ NOTE_E+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 64	},
		{ PAUSE		, 224	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 256	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 128	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_A+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_F+1+	5*OCTAVE, 256	},
		{ PAUSE		, 128	},
		{ NOTE_E+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 64	},
		{ PAUSE		, 224	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 256	},
		{ NOTE_F+1+	6*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 128	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	5*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	6*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	5*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_A+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_F+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	4*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_E+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_E+	4*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_D+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	4*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_D+1+	4*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	3*OCTAVE, 128	},
		{ PAUSE		, 64	},
		{ NOTE_B+	3*OCTAVE, 128	},
		{ PAUSE		, 256	},
		{ NOTE_A+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_A+1+	3*OCTAVE, 64	},
		{ PAUSE		, 32	},
		{ NOTE_B+	3*OCTAVE, 256	},
		}
};

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

/**
 * The currently playing long song. This is set by ::play_longsong().
 */
static const song_t *current_longsong;

/**
 * The id of the long song part on the Roomba. This value can be incremented using ::INCREMENT_ID.
 */
static uint8_t current_longsong_id;

/**
 * Flag that is used to indicate, if the last part of a song was loaded by ::continue_load_longsong().
 */
static uint8_t longsong_finish = 0;

/******************************************************************** Macros */

/**
 * Increments the value ::current_longsong_id and resets it to 0 if required.
 */
#define INCREMENT_ID() {	\
	current_longsong_id++;	\
	if (current_longsong_id >= 2) { /* Maybe use 2 instead of MAX_SONGS */	\
		current_longsong_id = 0;	\
	}	\
}

/********************************************************** Global functions */

/**
 * Plays a ::song_t with a total number of notes greater than ::MAX_LENGTH.
 * If the song contains equal or less than ::MAX_LENGTH notes, ::play_song() will
 * be called instead.
 *
 * This function loads parts of the song incrementally (a part consisting of maximum ::MAX_LENGTH notes)
 * and plays them. The user must call ::update_longsong() inside a `while(true)` loop,
 * to continue playing and loading the next parts of the song.
 *
 * @param song The song to play.
 */
static void play_longsong(const song_t *song) {
	if (song->length <= MAX_LENGTH) {
		play_song(song);
		return;
	}

	current_longsong = song;
	longsong_finish = 0;

	// Load the first part.
	INCREMENT_ID();
	continue_load_longsong(current_longsong_id);
	play_sound(current_longsong_id);

	// Pre-load the second part. ::update_longsong() will start to play this.
	INCREMENT_ID();
	continue_load_longsong(current_longsong_id);
}

/**
 * Continues playing a long song started by ::play_longsong().
 * Accessed variables:
 * - ::current_longsong_id
 * - ::longsong_finish
 * - ::current_longsong
 */
void update_longsong() {
	if (current_longsong != NULL) {
		if (query_value(PACKET_SONG_PLAYING) == 0) {
			play_sound(current_longsong_id);

			if (longsong_finish) {
				current_longsong = NULL;
			} else {
				INCREMENT_ID();
				continue_load_longsong(current_longsong_id);
			}
		}
	}
}

/**
 * Determines if a longsong is currently playing.
 * This does not work with short songs.
 *
 * @return ::true, if a longsong is currently playing; otherwise ::false.
 */
inline uint8_t is_playing() {
	return current_longsong != NULL;
}

/**
 * Plays the given song on the Roomba.
 * If the song is not yet stored on the Roomba, it will be transfered first.
 * Otherwise it will a simple play song by id is issued.
 *
 * @param song The song to play. If this is a long song, ::play_longsong() will be called instead.
 */
void play_song(const song_t *song) {
	/**
	 * Keeps track of each song that is currently stored on the Roomba with its associated song id.
	 * The song id is the index of this array, while the values are pointers to the songs.
	 */
	static song_t *current_songs[MAX_SONGS];

	/**
	 * The number of songs currently stored on the Roomba.
	 * This variable also indicates, which id to use for the next song that will
	 * be transfered to the Roomba.
	 */
	static uint8_t num_songs_loaded = 0;

	if (song->length > MAX_LENGTH) {
		// Parts of long songs are stored as song id 0 and 1.
		current_songs[0] = NULL;
		current_songs[1] = NULL;
		play_longsong(song);
		return;
	} else {
		// Do not load a new song with an index, that may currently be played.
		// This crashes the Roomba hard!
		if (current_longsong != NULL && num_songs_loaded < 2) {
			num_songs_loaded = 2;
		}

		current_longsong = NULL;
	}

	uint8_t i = 0;
	uint16_t duration_tmp = 0;

	// Is the song on the Roomba?
	for (i = 0; i < MAX_SONGS; ++i) {
		if (current_songs[i] == song) {
			break;
		}
	}

	// Song not yet on the Roomba. Load it first.
	if (i == MAX_SONGS) {
		uart_write_byte(CMD_SONG);
		uart_write_byte(num_songs_loaded);
		uart_write_byte(song->length);

		for (i = 0; i < song->length; ++i) {
			duration_tmp = calculate_seconds(song->notes[i].duration, song);
			if (duration_tmp > 0xff) {
				// TODO: do not cut off song lengths; create multiple sounds instead.
				duration_tmp = 0xff;
			}

			uart_write_byte(song->notes[i].note);
			uart_write_byte(duration_tmp);
		}

		// i contains the song id. must be set at exactly this position to work.
		i = num_songs_loaded;

		// Increase number of stored songs. if this value reaches ::MAX_SONGS, it is reset to 0.
		// A song will be overwritten only after ::MAX_SONGS other songs were loaded after it.
		num_songs_loaded++;
		if (num_songs_loaded >= MAX_SONGS) {
			num_songs_loaded = 0;
		}
	}

	// play it
	uart_write_byte(CMD_PLAY);
	uart_write_byte(i);
}

/*********************************************************** Local functions */

/**
 * Plays a song that is already stored on the Roomba.
 * @param id The id of the song to play.
 */
static void play_sound(uint8_t id) {
	uart_write_byte(CMD_PLAY);
	uart_write_byte(id);
}

/**
 * Loads the next (not yet loaded) part of ::current_longsong to the Roomba.
 * Once the last part is loaded, the variable ::longsong_finish will be set to indicate the song end.
 *
 * Accessed variables:
 * - ::longsong_finish will be set to 1 after the last part was loaded.
 *
 * @param song_id The song id on the Roomba, that is used for the loaded part.
 */
static void continue_load_longsong(uint8_t song_id) {
	static uint16_t load_next_note = 0;	//!< The next note id to load. Everything below is already loaded.
	uint16_t duration_tmp = 0;

	// calculate the number of notes to load next.
	int16_t len = current_longsong->length - load_next_note;
	if (len > MAX_LENGTH) {
		len = MAX_LENGTH;
	} else if (len <= 0) {
		load_next_note = 0;
		longsong_finish = 1;
		return;
	}

	// load notes to Roomba.
	uart_write_byte(CMD_SONG);
	uart_write_byte(song_id);
	uart_write_byte(len);

	len += load_next_note;
	for (; load_next_note < len; ++load_next_note) {
		duration_tmp = calculate_seconds(current_longsong->notes[load_next_note].duration, current_longsong);
		if (duration_tmp > 0xff) {
			// TODO: do not cut off song lengths; create multiple sounds instead.
			duration_tmp = 0xff;
		}

		// write to Roomba
		uart_write_byte(current_longsong->notes[load_next_note].note - OCTAVE);
		uart_write_byte(duration_tmp);
	}

	if (load_next_note >= current_longsong->length) {
		load_next_note = 0;
		longsong_finish = 1;
	}
}

/**
 * Calculates a value specified in seconds multiplied by 64 from a note duration and the song time signature and bpm.
 * This value can be directly used as note duration for the Roomba.
 *
 * @param duration The duration of the song, specified with ::FULL_NOTE, ::HALF_NOTE, etc.
 * @param song The song that contains timing information used for the calculation.
 * @return The duration converted to a value in 1/64 seconds based on the songs timing.
 */
static inline uint16_t calculate_seconds(uint8_t duration, const song_t *song) {
	return (uint16_t)(song->beats * duration * 240 / song->bpm);
}
