#include "board.h"
#include "led.h"
#include "pio.h"
#include "roomba.h"
#include "tools.h"
#include "uart.h"
#include "roombatools.h"

void write_7seg(unsigned char a,unsigned char b,unsigned char c,unsigned char d)
{
	uart_write_byte(CMD_7SEG_ASCII);
	uart_write_byte(a);
	uart_write_byte(b);
	uart_write_byte(c);
	uart_write_byte(d);
}

void leds(uint8_t led, uint8_t color, uint8_t intensity) {
	uart_write_byte(CMD_LEDS);
	uart_write_byte(led);
	uart_write_byte(color);
	uart_write_byte(intensity);
}

void sleds(uint8_t days, uint8_t symbols)
{
	uart_write_byte(CMD_SCHEDULE_LED);
	uart_write_byte(days);
	uart_write_byte(symbols);
}

char get_infrared()
{
	uart_write_byte(CMD_SENSORS);
	uart_write_byte(52);
	return uart_read_byte();
}

void sseg(uint16_t val) {
	uint8_t a, b, c, d;

	a = (val&0xF000)>>12;
	if(a > 9)	a += 55;
	else		a += '0';

	b = (val&0x0F00)>>8;
	if(b > 9)	b += 55;
	else		b += '0';

	c = (val&0x00F0)>>4;
	if(c > 9)	c += 55;
	else		c += '0';

	d = val&0x000F;
	if(d > 9)	d += 55;
	else		d += '0';

	write_7seg(a, b, c, d);
}

void init_roomba() {
	BUTTON_WAIT(0);
	LED = led_vals[0] | led_vals[5];
	msleep(1000);
	LED = led_vals[1] | led_vals[4];

	uart_write_byte(CMD_START);
	uart_write_byte(CMD_CONTROL);
	uart_write_byte(CMD_FULL);

	msleep(200);

	leds(0x1,0,0);
	sleds(0x1,0);
	BUTTON_WAIT(1);
	leds(0,0,0);
	sleds(0,0);
	LED = 0x0;
}

void drive_straight(int16_t vel) {
	uart_write_byte(CMD_DRIVE);
	uart_write_byte(vel >> 8);
	uart_write_byte(vel & 0xFF);
	uart_write_byte(0x7F);
	uart_write_byte(0xFF);
}

/**
 * Drive with the given velocity and radius.
 * @param vel The velocity to drive with.
 * @param dir The radius to drive. Positive values are left turns, negative values are right turns.
 * Use the special values 1 or -1 to turn in place.
 */
void drive(int16_t vel, int16_t dir) {
	uart_write_byte(CMD_DRIVE);
	uart_write_byte(vel >> 8);
	uart_write_byte(vel & 0xFF);
	uart_write_byte(dir >> 8);
	uart_write_byte(dir & 0xFF);
}

void stop() {
	uart_write_byte(CMD_DRIVE);
	uart_write_byte(0x00);
	uart_write_byte(0x00);
	uart_write_byte(0x7F);
	uart_write_byte(0xFF);
}

