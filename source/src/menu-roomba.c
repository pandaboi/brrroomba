/****************************************************************** Includes */

#include <stdint.h>
#include <menu-roomba.h>
#include <querylist.h>
#include <segment7.h>
#include <roomba.h>
#include <roombatools.h>
#include <tools.h>
#include <brrroomba.h>

/******************************************************************* Defines */

/******************************************************* Function prototypes */

static uint8_t display_menu_helper(const menu_t*, const uint8_t, const uint8_t, const uint8_t);

/************************************************************** Global const */

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

static char string[MAX_MENU_NAME + 2];
static uint8_t i = 0;
volatile static uint8_t btns = 0;
volatile static uint8_t ir = 0;

/******************************************************************** Macros */


/********************************************************** Global functions */

/**
 * Displays the given menu on the 7 segment display and allows the user to select menu items.
 *
 * The menu is controlled by the following buttons on the Roomba and the IR remote:
 * - ::BTN_OK/::IR_DATA.ok:		selects an item
 * - ::BTN_UP/::IR_DATA.up:		selects the previous item (cycle)
 * - ::BTN_DOWN/::IR_DATA.down:	selects the next item (cycle)
 * - ::BTN_BACK/::IR_DATA.stop:	exit menu (may be a submenu or top-level menu)
 *
 * @param menu The menu to display. If the menu is invalid (NULL pointer or no items defined) the function returns immediatly.
 * @param display_level If set to true, the current level of the menu will be displayed using the weekday LEDs.
 *        The Level increases with each submenu. The root-menu-roomba.has level 1, the first submenu level 2, etc.
 * @param display_num If set to true, each menu entry is prefixed with its number/position in the menu.
 * @return One of ::MENU_CONTINUE, ::MENU_BACK, ::MENU_EXIT or any other custom code.
 * Only ::MENU_EXIT has the special meaning of exiting the whole menu.
 */
uint8_t display_menu(const menu_t* menu, const uint8_t display_level, const uint8_t display_num) {
    return display_menu_helper(menu, display_level, display_num, 0);
}

/**
 * @return ::MENU_BACK
 */
uint8_t menu_back() {
	return MENU_BACK;
}

/**
 * @return ::MENU_EXIT
 */
uint8_t menu_exit() {
	return MENU_EXIT;
}

/*********************************************************** Local functions */

/**
 * @copydoc display_menu()
 * @param level The level to use for the menu
 */
static uint8_t display_menu_helper(const menu_t* menu, const uint8_t display_level, const uint8_t display_num, const uint8_t level) {
	uint8_t selection = 0;		// must be local, as this function calls itself
	uint8_t prev_selection = 1;	// init with something different to ::selection to update 7seg on first run.
	uint8_t return_code = 0x00;	// return code from submenu or function.

	if (menu == NULL || menu->size == 0) {
		return MENU_CONTINUE;
	}

	if (display_level) {
		sleds(1 << level, 0x00);
	}

	while (true) {
		// display new string
		if (prev_selection != selection) {
			if (display_num) {
				string[0] = '0' + (selection + 1);
				string[1] = ' ';
				i = 0;
				for (i = 0; menu->items[selection].name[i] != '\0'; ++i) {
					string[2 + i] = menu->items[selection].name[i];
				}
				string[2 + i] = '\0';
				set_7seg_string(string);
			} else {
				set_7seg_string(menu->items[selection].name);
			}

			prev_selection = selection;
		}

		// roll string
		update_7seg();


		// Menu control
		query_input[0] = PACKET_BUTTONS;
		query_input[1] = PACKET_IR_OMNI;

		query_list(2);

		btns = query_output[0];
		ir = query_output[1];

		if (IS_OK(btns, ir)) {
			ROOMBA_RELEASE_OK();

			// select item
			if (menu->items[selection].fn != NULL) {
				if (display_level) {
					sleds(0x00, 0x00);
				}

				return_code = menu->items[selection].fn();

				// set prev_selection to a different value, to force displaying the correct menu item name.
				prev_selection++;

				if (display_level) {
					sleds(1 << level, 0x00);
				}

				switch(return_code) {
					case MENU_CONTINUE:
						break;
					case MENU_BACK:
					case MENU_EXIT:
						return return_code;
					default:
						// return any custom code.
						return return_code;
				}
			}

			if (menu->items[selection].submenu != NULL) {
				return_code = display_menu_helper(menu->items[selection].submenu, display_level, display_num, level + 1);

				// set prev_selection to a different value, to force displaying the correct menu item name.
				prev_selection++;

				if (display_level) {
					sleds(1 << level, 0x00);
				}

				switch(return_code) {
					// MENU_BACK was already handled on the exited submenu.
					case MENU_CONTINUE:
					case MENU_BACK:
						break;
					case MENU_EXIT:
						return return_code;
					default:
						return return_code;
				}
			}
		} else if (IS_UP(btns, ir)) {
			// item up
			if (selection > 0) {
				--selection;
			} else {
				selection = menu->size - 1;
			}

			ROOMBA_RELEASE_UP();
		} else if (IS_DOWN(btns, ir)) {
			// item down
			if (selection < menu->size - 1) {
				++selection;
			} else {
				selection = 0;
			}

			ROOMBA_RELEASE_DOWN();
		} else if (IS_BACK(btns, ir)) {
			ROOMBA_RELEASE_BACK();
			return MENU_CONTINUE;
		}
	}

	return MENU_CONTINUE;
}
