/****************************************************************** Includes */

#include <chuck_norris.h>
#include <stdint.h>
#include <querylist.h>
#include <segment7.h>
#include <roomba.h>
#include <roombatools.h>
#include <tools.h>
#include <statemachine.h>
#include <calibrate.h>
#include <brrroomba.h>
#include <ir.h>

/******************************************************************* Defines */

/// Global Chuck Norris Data. TODO: change for Chuck Norris.
#define DATA	brrroomba.game

#define CHUCK_NORRIS_VELOCITY   250

/******************************************************* Function prototypes */

static uint8_t st_chuck_norris();
static uint8_t st_stop();
static uint8_t st_turn_random();
static uint8_t st_turn_bumper();

static inline void update_drive();

/************************************************************** Global const */

/********************************************************** Local variables */

volatile static uint8_t btns = 0;
volatile static uint8_t bumper = 0;
volatile static uint8_t ir = 0;
volatile static uint16_t cliff_front_left = 0;
volatile static uint16_t cliff_front_right = 0;

/// A random variable. Use this to get a truly random value.
static uint8_t random = 0;

/// Should a song be played continuously?
uint8_t do_play_song = 1;

/*********************************************************** Local functions */

/**
 * All possible Chuck Norris states.
 */
enum state_t
{
    ST_CHUCK_NORRIS,
    ST_STOP,
    ST_EXIT,
    ST_TURN_RANDOM,
    ST_TURN_BUMPER,
};

static transition_t transitions[] =
{
	{ ST_CHUCK_NORRIS,			&st_chuck_norris			},
	{ ST_STOP,					&st_stop					},
	{ ST_TURN_BUMPER,			&st_turn_bumper				},
	{ ST_TURN_RANDOM,			&st_turn_random				},
};

/**
 * State machine for the awesome Chuck Norris.
 */
void chuck_norris_main()
{
    DATA.velocity = CHUCK_NORRIS_VELOCITY;

    sm_start(
        ST_CHUCK_NORRIS, ST_EXIT,
        transitions, sizeof(transitions) / sizeof(transition_t*)
    );
}


/**
 * Checks if the program should be stopped and transission to another state (preferably to ::ST_STOP).
 *
 * Accessed variables:
 * - ::btns
 * - ::ir
 *
 * @return Boolean value, indicating, whether the program should be stopped (::true) or not (::false).
 */
static inline uint8_t check_stop() {
	if ((btns & (BTN_OK | BTN_BACK)) || ir == IR_DATA.stop) {
		stop();
		ROOMBA_BUTTON_RELEASE(BTN_CLEAN);
		ROOMBA_IR_RELEASE(IR_DATA.stop);
		return true;
	}

	return false;
}

/**
 * Perform global actions, like updating the 7 segment display and playing songs.
 */
static inline void handle_global()
{
    if (do_play_song) {
    	if (!is_playing()) {
    		play_song(&bombermansong);
    	}

        update_longsong();
    }

    update_7seg();
}

/**
 * Let the Roomba drive between two lines.
 * @return
 * - ::ST_TURN_RANDOM
 * - ::ST_TURN_BUMPER
 * - ::ST_STOP
 */
static uint8_t st_chuck_norris()
{
	set_7seg_string("CHUCK");

	DATA.radius = 0;

    while (true)
    {
    	handle_global();

        query_input[0] = PACKET_BUTTONS;
        query_input[1] = PACKET_CLIFF_FRONT_LEFT;
        query_input[2] = PACKET_CLIFF_FRONT_RIGHT;
        query_input[3] = PACKET_BUMPER;
        query_input[4] = PACKET_IR_OMNI;

        query_list(5);

        btns = query_output[0];
        cliff_front_left = query_output[1];
        cliff_front_right = query_output[2];
        bumper = query_output[3];
        ir = query_output[4];

        // update our very random variable used by ::st_turn_random().
        random++;
        if (random > 7) {
        	random = 0;
        }

        if (IS_UP(btns, ir)) {
        	do_play_song = !do_play_song;
        	if (!do_play_song) {
        		// playing another sound does not stop the currently playing one.
        		// must still try to play a song to reset the previously playing song.
        		play_song(&empty_sound);
        	}
        	ROOMBA_RELEASE_UP();
        }

        if (ON_TRACK(cliff_front_left) || ON_TRACK(cliff_front_right)) {
        	return ST_TURN_RANDOM;
        }

		if (bumper & (BUMPER_LEFT | BUMPER_RIGHT)) {
		    return ST_TURN_BUMPER;
		}

		if (check_stop()) {
			return ST_STOP;
		}

		update_drive();
    }

    return ST_STOP;
}

/**
 * Let the Roomba turn with a random angle between 90 and 160 degrees.
 * @return
 * - ::ST_PREVIOUS
 * - ::ST_STOP
 */
static uint8_t st_turn_random() {
	// reset angle
	query_value(PACKET_ANGLE);

	int16_t angle = 0;
	int16_t to_drive_angle = get_angle(90 + 10*random);

	if (random & 1) {
		DATA.radius = 1;
	} else {
		DATA.radius = -1;
	}

	while (true) {
    	handle_global();

		query_input[0] = PACKET_BUTTONS;
		query_input[1] = PACKET_ANGLE;
		query_input[2] = PACKET_IR_OMNI;

		query_list(3);

		btns = query_output[0];
		angle += query_output[1];
		ir = query_output[2];

		if (ABS(angle) >= to_drive_angle) {
			break;
		}

		if (check_stop()) {
			return ST_STOP;
		}

		update_drive();
	}

	return ST_PREVIOUS;
}

/**
 * Let the Roomba turn until the bumper are no longer pressed.
 * @return
 * - ::ST_PREVIOUS
 * - ::ST_STOP
 */
static uint8_t st_turn_bumper() {
	while (true) {
    	handle_global();

		query_input[0] = PACKET_BUTTONS;
		query_input[1] = PACKET_BUMPER;
		query_input[2] = PACKET_IR_OMNI;

		query_list(3);

		btns = query_output[0];
		bumper = query_output[1];
		ir = query_output[2];

		// Check bumpers
		if (bumper & BUMPER_LEFT) {
			DATA.radius = -1;
		} else if (bumper & BUMPER_RIGHT) {
			DATA.radius = 1;
		} else {
			break;
		}

		if (check_stop()) {
			return ST_STOP;
		}

		update_drive();
	}

	return ST_PREVIOUS;
}

/**
 * State: Chuck Norris stopped his path of destiny.
 * Returns to the main menu.
 *
 * @return
 * - ::ST_EXIT
 */
static uint8_t st_stop()
{
	DATA.velocity = 0;
	update_drive();
    return ST_EXIT;
}

/**
 * Let the Roomba drive according to the values ::DATA.velocity and ::DATA.radius.
 * The drive command is only issued, if velocity or radius changed since the last call.
 *
 * Accessed variables:
 * - ::DATA.velocity
 * - ::DATA.radius
 */
static inline void update_drive() {
	// static variables are initialized only once across function calls.
	static int16_t prev_radius = 0;
	static int16_t prev_velocity = 0;

	// drive according to calculated radius
	if (DATA.radius != prev_radius || DATA.velocity != prev_velocity)
	{
		prev_radius = DATA.radius;
		prev_velocity = DATA.velocity;
		drive(DATA.velocity, DATA.radius);
	}
}
