/****************************************************************** Includes */

#include <querylist.h>
#include <segment7.h>

#include <brrroomba.h>
#include <board.h>
#include <roomba.h>
#include <tools.h>
#include <uart.h>
#include <pio.h>
#include <led.h>

/******************************************************************* Defines */

/**
 * Maximum number of packets used by the query list command.
 */
#define NUM_PACKETS 7

/******************************************************* Function prototypes */

/************************************************************** Global const */

/**
 * Contains the number of bytes of the response for each packet id.
 * The packet id equals the index in this array.
 *
 * Example: Packet ID 17: packet_lengths[17] = 1 (→ response of 1 byte)
 */
static const uint8_t packet_lengths[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	1,	// Bumps and Wheel Drops
	1,
	1,
	1,	// Packet ID 10
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,	// Buttons (18)
	2,	// Distance (19)
	2,	// Angle (20)
	1,
	2,
	2,
	1,
	2,
	2,
	2,
	2,
	2,
	2,	// Cliff Front Right Signal (30)
	2,
	3,	// Unused
	3,	// Unused
	1,
	1,
	1,
	1,
	1,
	2,	// Requested Velocity (39)
	2,
	2,
	2,
	2,
	2,
	1,
	2,
	2,
	2,
	2,
	2,	// Light Bump Front Right Signal (50)
	2,
	1,	// Infrared Character Left (52)
	1,	// Infrared Character Right (53)
	2,	// Left Motor Current (54)
	2,
	2,
	2,
	1,
};

/**
 * Indicates, whether the response to a packet id is treated as signed (1) or unsigned (0).
 * The packet id equals the index in this array.
 *
 * Example: Packet ID 17: packet_signed[17] = 0 (→ unsigned)
 */
static const uint8_t packet_signed[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,	// Bumps and Wheel Drops
	0,
	0,
	0,	// Packet ID 10
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	1,	// Distance (19)
	1,	// Angle (20)
	0,
	0,
	1,
	1,
	0,
	0,
	0,
	0,
	0,
	0,	// Cliff Front Right Signal (30)
	0,
	0,	// Unused
	0,	// Unused
	0,
	0,
	0,
	0,
	0,
	1,	// Requested Velocity (39)
	1,
	1,
	1,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,	// Light Bump Front Right Signal (50)
	0,
	0,	// Infrared Character Left (52)
	0,	// Infrared Character Right (53)
	1,	// Left Motor Current (54)
	1,
	1,
	1,
	0,
};

/********************************************************** Global variables */

/*************************************************************** Local const */

static uint8_t i = 0;

/*********************************************************** Local variables */

/**
 * List of Packet IDs, that are used by ::query_list(uint8_t).
 */
volatile uint8_t query_input[NUM_PACKETS];

/**
 * Response to the Packet IDs in ::query_input is written to this array.
 */
volatile int32_t query_output[NUM_PACKETS];

/******************************************************************** Macros */


/********************************************************** Global functions */

/**
 * Sends the first num values of query_input as Query List command.
 * The result is written to ::query_output.
 *
 * If the number of packets is greater than ::NUM_PACKETS, this function
 * displays "ERNO" on the seven segment display and waits for an Ok from the user.
 * No values will be queried in this case.
 * 
 * @param num The number of Packet IDs from ::query_input to send. This value must not exceed ::NUM_PACKETS.
 */
void query_list_plain(uint8_t num) {
	uint8_t j = 0;
	int32_t help = 0;
	int32_t read = 0;

	if (num > NUM_PACKETS) {
		set_7seg_string("ERNO");
		ROOMBA_WAIT_OK();
		return;
	}

	uart_write_byte(CMD_QUERY_LIST);
	uart_write_byte(num);

	for (i = 0; i < num; ++i) {
		uart_write_byte(query_input[i]);
	}

	for (i = 0; i < num; ++i) {
		j = 0;
		help = 0;
		if (packet_signed[query_input[i]]) {
			// set all bits, including the signed bit.
			// this is required for all responses, that are signed.
			// unsigned responses cut off all leading 0xFF.. (2-complement).
			help = uart_read_byte();
			j++;
		}

		for (; j < packet_lengths[query_input[i]]; ++j) {
			// must use only the last 8 bit, as read_byte() may be in 2-complement form.
			read = uart_read_byte();
			help <<= 8;
			help |= (read & 0xff);
		}

		query_output[i] = help;
	}
}

/**
 * Queries the Roomba for the given packet id.
 * @param packet_id The requested packet id.
 * @return The response for the requested packet.
 */
int32_t query_value(uint8_t packet_id) {
	query_input[0] = packet_id;
	query_list(1);

	return query_output[0];
}

/**
 * Thin wrapper around ::query_list_plain() that removes the effects of IR mapped button presses.
 * If ::query_input contains both ::PACKET_BUTTONS and any of ::PACKET_IR_OMNI, ::PACKET_IR_LEFT or ::PACKET_IR_RIGHT,
 * and ::PACKET_BUTTONS has values set by a mapped IR command, these pressed buttons will be removed from ::PACKET_BUTTONS.
 * 
 * If ::query_input contains more than one ::PACKET_BUTTONS, only the first packet will be modified.
 *
 * @copydoc query_list_plain(uint8_t)
 */
void query_list(uint8_t num) {
	static uint8_t wait_for_empty_btns = 0;

	int16_t button_id = -1;
	int16_t ir_id = -1;

	for (i = 0; i < num; ++i) {
		if (query_input[i] == PACKET_BUTTONS) {
			button_id = i;
		} else if (query_input[i] == PACKET_IR_LEFT || query_input[i] == PACKET_IR_OMNI || query_input[i] == PACKET_IR_RIGHT) {
			ir_id = i;
		}
	}

	// Call our normal query_list.
	query_list_plain(num);

	// The IR was requested. Must do some checks to suppress physical button presses.
	if (ir_id != -1) {
		// Check which buttons should be ignored.
		switch (query_output[ir_id]) {
		case IROBOT_580_DOCK:
		case IROBOT_580_MAX:
			// mapped to ::BTN_DOCK
			wait_for_empty_btns = BTN_DOCK;
			break;
		case IROBOT_580_SPOT:
			// mapped to ::BTN_SPOT
			wait_for_empty_btns = BTN_SPOT;
			break;
		case IROBOT_580_SMALL:
		case IROBOT_580_MEDIUM:
		case IROBOT_580_LARGE:
			// mapped to ::BTN_CLEAN
			wait_for_empty_btns = BTN_CLEAN;
			break;
		}
	}

	// Set button pressed to not-pressed, if it is an IR mapped button.
	if (button_id != -1) {
		// Turn all mapped buttons off.
		// This is a stupid/simple fix, for the situation where two differently mapped
		// IR buttons are pressed nearly concurrently.
		// Without this fix, only one of these buttons will be turned off. The other is free to cause chaos.
		if (wait_for_empty_btns) {
			wait_for_empty_btns = BTN_DOCK | BTN_SPOT | BTN_CLEAN;
		}

		if (query_output[button_id] & wait_for_empty_btns) {
			query_output[button_id] &= (~wait_for_empty_btns);
		} else {
			wait_for_empty_btns = 0x00;
		}
	}
}

/*********************************************************** Local functions */

