/* see http://stackoverflow.com/questions/1647631/c-state-machine-design */

/****************************************************************** Includes */

#include <statemachine.h>
#include <tools.h>
#include <roombatools.h>

/******************************************************************* Defines */

/**
 * The maximum number of states to keep when using the special state ::ST_PREVIOUS.
 */
#define MAX_PREV_STATES		10

/******************************************************* Function prototypes */

/************************************************************** Global const */

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

/******************************************************************** Macros */

/********************************************************** Global functions */

/**
 * Start the state machine defined by the given parameters.
 *
 * @param start_state The first state that will be activated.
 * @param stop_state The state machine will be stopped, once it reaches this state.
 * @param transitions The state transition lookup table.
 * @param count_transitions The number of transitions defined in the parameter transitions.
 */
void sm_start(uint8_t start_state, uint8_t stop_state, transition_t* transitions, uint8_t count_transitions) {
	uint8_t state = start_state;
	uint8_t i = 0;
	uint8_t newstate = 0;
	uint8_t found = 0;

	uint16_t prev_state = 0;
	uint8_t prev_states[MAX_PREV_STATES];
	for (i = 0; i < MAX_PREV_STATES; ++i) {
		prev_states[i] = 0x00;
	}

	prev_states[0] = state;

	while (state != stop_state) {
		found = 0;
		for (i = 0; i < count_transitions; ++i) {
			if (transitions[i].state == state) {
				// display current state as week days.
				sleds(state, 0x00);
				newstate = transitions[i].fn();
				switch(newstate) {
					case ST_PREVIOUS:
						// set the new state from a previous one.
						prev_state--;
						state = prev_states[prev_state % MAX_PREV_STATES];
						break;
					default:
						prev_state++;
						prev_states[prev_state % MAX_PREV_STATES] = newstate;
						state = newstate;
						break;
				}

				found = 1;
				break;
			}
		}

		if (!found) {
			return;
		}
	}
}

/*********************************************************** Local functions */
