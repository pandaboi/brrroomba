/****************************************************************** Includes */

#include <segment7.h>

#include <board.h>
#include <led.h>
#include <pio.h>
#include <roomba.h>
#include <tools.h>
#include <uart.h>

// Timer includes
#include <or32intrinsics.h>
#include <spr-defs.h>
#include <timer.h>

/******************************************************************* Defines */

#define UART_SLEEP 30

/**
 * Maximum number of characters, that can be displayed (rolled) on the 7 segement display.
 */
#define MAX_STRING_LENGTH 16

#define ASCII_MINUS 45
#define ASCII_A 97		//!< uppercase A	CHANGEME
#define ASCII_a 0x41		//!< lowercase a	CHANGEME
#define ASCII_0 0x30		//!< number 0
#define ASCII_BLANK 0x20	//!< space ' '
#define ASCII__ 95		//!< underscore _	CHANGEME

/******************************************************* Function prototypes */

static void show_7seg_rolled(uint8_t force);
static void show_7seg_current();
static void show_7seg_rolled_callback();
static void set_7seg(uint32_t val);
static uint8_t get_hexascii_for_halfbyte(uint8_t byte);
static uint8_t get_decimal_at_position(int32_t val, uint8_t pos);
static int32_t complement2(int32_t value);

/************************************************************** Global const */

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

/**
 * The string that is displayed on the 7 segment display.
 * This array contains ascii characters that can be displayed directly
 * on the Roomba using the command ::CMD_7SEG_ASCII.
 */
static uint8_t segment_string[MAX_STRING_LENGTH];

/**
 * The length of the string in ::segment_string.
 */
static uint8_t segment_string_length = 0;

/**
 * The first character from ::segment_string that is displayed.
 */
static uint8_t segment_string_first = 0;

/**
 * The first character that was really displayed on the 7 segment display.
 * The value ::segment_string_first is updated by ::show_7seg_rolled_callback()
 * but the updated substring is not written to the Roomba.
 * The function ::update_7seg() writes the appropriate substring to the
 * Roomba according to this value and ::segment_string_first.
 * If both values are equal, nothing needs to be done.
 */
static uint8_t segment_string_first_threadsafe = 0;

/******************************************************************** Macros */


/********************************************************** Global functions */

/**
 * Updates the 7 segment display. This is required for rolled content.
 * This function should be called in each while(true) loop, i.e. as often
 * as possible.
 *
 * Problem:
 * A timer is effectivly a new thread.
 * When using threads, it must be ensured, that no two threads can concurrently
 * write/read data to/from the Roomba.
 * It is fatal, if one thread starts a query_list() command while another thread
 * starts and tries to set the 7 segment display. The value, that should be set
 * to the display may be appended to the QUERY command and interpreted as packet id.
 */
inline void update_7seg() {
	if (segment_string_first_threadsafe != segment_string_first) {
		segment_string_first_threadsafe = segment_string_first;
		show_7seg_current();
		msleep(100);
	}
}

/**
 * Displays the given string on the Roombas 7 segment display.
 * The values stored in the string should already be in ASCII form, that
 * is understood by the Roomba.
 *
 * @param string The string to display.
 * @param length The length of the string.
 */
void set_7seg_ascii(const uint8_t* string, uint8_t length) {
	if (length > MAX_STRING_LENGTH) {
		length = MAX_STRING_LENGTH;
	}

	segment_string_length = length;
	// TODO: array copy...anyone?
	for (; length > 0; --length) {
		segment_string[length - 1] = string[length - 1];
	}

	show_7seg_rolled(0);
}

/**
 * Displays the given string on the Roombas 7 segment display.
 * The values stored in the string will be converted from C char to an
 * ASCII, that is understood by the Roomba.
 *
 * @param string The string to display.
 */
void set_7seg_string(const char* string) {
	uint8_t changed = 0;	//!< did the displayed string changed compared to the previous?
	uint8_t length = 0;
	while (string[length] != '\0') {
		if (segment_string[length] != string[length]) {
			changed = 1;
		}
		segment_string[length] = string[length];
		length++;
	}

	if (length != segment_string_length) {
		changed = 1;
	}

	if (changed) {
		segment_string_length = length;

		show_7seg_rolled(0);
	}
}

/**
 * Displays the given value as hex number on the Roombas 7 segment display.
 *
 * @param val The value to display.
 */
void set_7seg_hex(int32_t val) {
	uint8_t pos = 0;
	if (val < 0) {
		val = complement2(val);
		segment_string[pos++] = ASCII_MINUS;
	}

	uint8_t digits = 0;
	int32_t tmp = val;	// val > 0 (complement2 already called)
	while (tmp) {
		++digits;
		tmp = tmp >> 4;
	}

	uint8_t changed = 0;	//!< did the displayed string changed compared to the previous?
	int8_t i;
	for (i = digits - 1; i >= 0; --i) {
		uint8_t new = get_hexascii_for_halfbyte((val >> (i * 4)) & 0xf);
		if (segment_string[pos] != new) {
			segment_string[pos] = new;
			changed = 1;
		}
		pos++;
	}

	// display at least a 0
	if (digits == 0) {
		if (segment_string[pos] != ASCII_0) {
			changed = 1;
		}
		segment_string[pos++] = ASCII_0;
	}

	if (pos != segment_string_length) {
		changed = 1;
	}

	if (changed) {
		segment_string_length = pos;

		show_7seg_rolled(0);
	}
}

/**
 * Displays the given value as decimal number on the Roombas 7 segment display.
 *
 * @param val The value to display.
 */
void set_7seg_dec(int32_t val) {
	uint8_t pos = 0;
	if (val < 0) {
		val = complement2(val);
		segment_string[pos++] = ASCII_MINUS;
	}

	uint8_t digits = 0;
	int32_t tmp = val;	// val > 0 (complement2 already called)
	while (tmp) {
		++digits;
		tmp /= 10;
	}

	uint8_t changed = 0;	//!< did the displayed string changed compared to the previous?
	int8_t i;
	uint8_t new = 0;
	for (i = digits - 1; i >= 0; --i) {
		new = get_hexascii_for_halfbyte(get_decimal_at_position(val, i));
		if (segment_string[pos] != new) {
			segment_string[pos] = new;
			changed = 1;
		}
		pos++;
	}

	// display at least a 0
	if (digits == 0) {
		if (segment_string[pos] != ASCII_0) {
			changed = 1;
		}
		segment_string[pos++] = ASCII_0;
	}

	if (pos != segment_string_length) {
		changed = 1;
	}

	if (changed) {
		segment_string_length = pos;

		show_7seg_rolled(0);
	}
}

/**
 * Displays the given value as binary number on the Roombas 7 segment display.
 *
 * @param val The value to display.
 */
void set_7seg_bin(int32_t val) {
	uint8_t pos = 0;
	if (val < 0) {
		val = -val;
		segment_string[pos++] = ASCII_MINUS;
	}

	uint8_t digits = 0;
	int32_t tmp = val;	// val > 0 (complement2 already called)
	while (tmp) {
		++digits;
		tmp = tmp >> 1;
	}

	uint8_t changed = 0;	//!< did the displayed string changed compared to the previous?
	int8_t i;
	uint8_t new = 0;
	for (i = digits - 1; i >= 0; --i) {
		new = get_hexascii_for_halfbyte((val >> i) & 0x1);
		if (segment_string[pos] != new) {
			segment_string[pos] = new;
			changed = 1;
		}
		pos++;
	}

	// display at least a 0
	if (digits == 0) {
		if (segment_string[pos] != ASCII_0) {
			changed = 1;
		}
		segment_string[pos++] = ASCII_0;
	}

	if (pos != segment_string_length) {
		changed = 1;
	}

	if (changed) {
		segment_string_length = pos;

		show_7seg_rolled(0);
	}
}

/*********************************************************** Local functions */

static void first_callback() {
	uint32_t ticks = 0x1312d00; // 0.5 * 40 MHz == 500ms
	//ticks *= 2;	// 1s
	_mtspr(SPR_TTMR, SPR_TTMR_RT | SPR_TTMR_IE | (ticks & SPR_TTCR_PERIOD));
	register_timer_cb(show_7seg_rolled_callback);
	enable_timer();
}

/**
 * Displays the value stored in segment_string on the 7 segment display.
 * If the segment_string_length is greater than 4, the string is rolled.
 * Example: Display -64533 (length of 6 characters):
 * |-645|
 * |6453|
 * |4533|
 * |533 |
 * |33 -|
 * |3 -6|
 * | -64|
 * |-645|
 *
 * @param force If true, the value is always rolled, even if it would fit.
 */
static void show_7seg_rolled(uint8_t force) {
	static uint8_t timer_running = 0;

	segment_string_first = 0;
	show_7seg_current();

	// Only one timer can be active at any point of time.
	// disable_timer() is only called if a timer was previously started by this function.
	// Thus, any other function using timers may not be interrupted by this function.
	if (force || segment_string_length > 4) {
		uint32_t ticks = 0x1312d00; // 0.5 * 40 MHz == 500ms
		ticks *= 4;	// 2s
		_mtspr(SPR_TTMR, SPR_TTMR_RT | SPR_TTMR_IE | (ticks & SPR_TTCR_PERIOD));
		register_timer_cb(first_callback);
		enable_timer();
		timer_running = 1;
	} else if (timer_running) {
		disable_timer();
		timer_running = 0;
	}
}

/**
 * Displays the currently selected (sub)string from segment_string starting
 * at position segment_string_first.
 * This should be called by a timer.
 */
static void show_7seg_current() {
	uint8_t start = segment_string_first;
	uint32_t val = 0;


	uint8_t pos = 0;
	if (start >= segment_string_length) {
		val <<= 8;
		val |= ASCII_BLANK;
		pos++;
	}
	if (start >= segment_string_length) {
		start = 0;
	}

	// fill first x fields with blanks
	for (; pos < 4 - segment_string_length; ++pos) {
		val <<= 8;
		val |= ASCII_BLANK;
	}

	uint8_t i = 0;
	uint8_t blanks = 0;
	for (; pos < 4; ++pos) {
		val <<= 8;
		if ((start + i + blanks) == segment_string_length) {
			// insert blank between last and first position of string
			val |= ASCII_BLANK;
			blanks++;
		} else {
			val |= segment_string[(start + i) % segment_string_length] & 0xff;
			i++;
		}
	}

	set_7seg(val);
}

/**
 * Timer callback to move the displayed value on the 7 segment display one to the left.
 * This callback only increments the value ::segment_string_first.
 * Because of threading issues, it is not safe to write the new value to
 * the display in this callback. ::update_7seg() should be called manually.
 */
static void show_7seg_rolled_callback() {
	segment_string_first = (segment_string_first + 1) % (segment_string_length + 1);
	// NOTE: only write value to display, if no other uart_write/read is in progress.
	// => mutex/semaphore required! threading issue!
	// The function update_7seg() should be used to update the display.
	//show_7seg_current(segment_string_first, 1);
	_mtspr(SPR_TTMR, _mfspr(SPR_TTMR) & ~SPR_TTMR_IP);
}

/**
 * Writes the given value to the 7 segment display. The high byte will
 * be displayed on the left, the low byte on the right.
 */
static void set_7seg(uint32_t val) {
	//msleep(UART_SLEEP);
	uart_write_byte(CMD_7SEG_ASCII);
	uart_write_byte((val & 0xff000000) >> 24);
	uart_write_byte((val & 0x00ff0000) >> 16);
	uart_write_byte((val & 0x0000ff00) >>  8);
	uart_write_byte((val & 0x000000ff) >>  0);
}

/**
 * Gets an ASCII character, representing the lower part (0x0f) of the given byte.
 */
static uint8_t get_hexascii_for_halfbyte(uint8_t byte) {
	byte &= 0x0f;
	if (byte < 0xa) {
		return ASCII_0 + byte;
	} else {
		return ASCII_A + byte - 0xa;
	}
}

/**
 * Gets the digit of the given value (interpreted as decimal) at the given position.
 *
 * Example:
 * get_decimal_at_position(255, 0) = 5
 * get_decimal_at_position(255, 2) = 2
 */
static uint8_t get_decimal_at_position(int32_t val, uint8_t pos) {
	int32_t res = val;

	// move significant number to last position
	while (pos > 0) {
		res /= 10;
		pos--;
	}

	// subtract everything >=10
	res -= (int32_t)(res / 10) * 10;

	return res;
}

/**
 * Calculates the 2-complement of the given value.
 * @return
 */
static int32_t complement2(int32_t value) {
	return ~value + 1;
}
