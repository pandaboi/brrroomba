/****************************************************************** Includes */

#include <brrroomba.h>

/******************************************************************* Defines */

/******************************************************* Function prototypes */

/************************************************************** Global const */

/**
 * Contains pre-defined IR remote controls.
 */
const remote_controller_t remote_controllers[] = {
	{
		IROBOT_580_FORWARD,	//!< Up
		IROBOT_580_BACK,	//!< Down
		IROBOT_580_LEFT,	//!< Left
		IROBOT_580_RIGHT,	//!< Right

		IROBOT_580_LARGE,	//!< Stop
		IROBOT_580_DOCK,	//!< OK
	},
	{
		IROBOT_580_MEDIUM,	//!< Up
		IROBOT_580_SMALL,	//!< Down
		IROBOT_580_TURNLEFT,	//!< Left
		IROBOT_580_TURNRIGHT,	//!< Right

		IROBOT_580_MAX,		//!< Stop
		IROBOT_580_SPOT,	//!< OK
	},
};

/**
 * A special remote control, that contains invalid IR signals.
 * Use this remote control to disable all IR input processing.
 */
const remote_controller_t no_remote = {
		0x01,
		0x01,
		0x01,
		0x01,

		0x01,
		0x01,
};

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

/******************************************************************** Macros */

/********************************************************** Global functions */

/*********************************************************** Local functions */
