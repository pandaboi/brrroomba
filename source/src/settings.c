/****************************************************************** Includes */

#include <settings.h>
#include <brrroomba.h>
#include <menu-roomba.h>
#include <querylist.h>
#include <roomba.h>
#include <segment7.h>

/******************************************************************* Defines */

/******************************************************* Function prototypes */

static uint8_t settings_select_ir();
static uint8_t settings_role();
static uint8_t settings_test_ir();
static uint8_t settings_test_buttons();

/************************************************************** Global const */

/********************************************************** Global variables */

menu_t settings_menu = {
	5,
	{
		{
			"IR Remote",
			&settings_select_ir,
			NULL,
		},
		{
			"Role",
			&settings_role,
			NULL,
		},
		{
			"IR Test",
			&settings_test_ir,
			NULL,
		},
		{
			"Button Test",
			&settings_test_buttons,
			NULL,
		},
		{
			"Load Defaults",
			&load_defaults,
			NULL,
		},
	}
};

/*************************************************************** Local const */

/*********************************************************** Local variables */

static volatile uint8_t btns = 0x00;
static volatile uint8_t ir = 0x00;

/******************************************************************** Macros */

/********************************************************** Global functions */

/**
 * Change the given value with regard to different constraints.
 *
 * The following macros are used to change the value:
 * - ::IS_UP Increase the value by the given step.
 * - ::IS_DOWN Decrease the value by the given step.
 * - ::IS_OK Confirm the value.
 * - ::IS_BACK Cancel the operation. Returns the same value as it was given by the user.
 *
 * @param value The value to change. This is the initial value.
 * @param step The step used to increase and decrease the value.
 * Use a negative value, to invert the up/down buttons.
 * @param min The minimum value that can be used.
 * @param max The maximum value that can be used.
 * @param continuous Boolean value indicating, whether the pressed button must be released before an additional
 * change can be made. true to enable continuous changing of the value while holding down the button.
 * @param display_fn A function used to display the current value on the Roomba. If this is NULL, it defaults to ::set_7seg_dec().
 *
 * @return The new value as set by the user.
 * This may be the same as the input value, if the user canceled this operation.
 */
int32_t change_value(int32_t value, int8_t step, int32_t min, int32_t max, uint8_t continuous, void (*display_fn)(int32_t)) {
	int32_t prev = value;

	// swap min and max if reverse direction is used.
	if (step < 0) {
		int32_t tmp = min;
		min = max;
		max = tmp;
	}

	if (display_fn == NULL)
		display_fn = &set_7seg_dec;

	while (true) {
		update_7seg();

		query_input[0] = PACKET_BUTTONS;
		query_input[1] = PACKET_IR_OMNI;
		query_list(2);
		btns = query_output[0];
		ir = query_output[1];

		display_fn(value);

		if (IS_UP(btns, ir)) {
			if (value < max)
				value += step;
			if (value > max)
				value = max;

			if (continuous)
				msleep(50);
			else
				ROOMBA_RELEASE_UP();
		} else if (IS_DOWN(btns, ir)) {
			if (value > min)
				value -= step;

			if (value < min)
				value = min;

			if (continuous)
				msleep(50);
			else
				ROOMBA_RELEASE_DOWN();
		} else if (IS_OK(btns, ir)) {
			ROOMBA_RELEASE_OK();
			return value;
		} else if (IS_BACK(btns, ir)) {
			ROOMBA_RELEASE_BACK();
			return prev;
		}
	}

	return prev;
}

/**
 * Loads default values for the ::brrroomba.
 */
uint8_t load_defaults() {

	brrroomba.calibration.raw_angle = 117;	// 500 velocity
	brrroomba.calibration.raw_distance = 125; // 300 velocity -- 127 with 200 velocity

	brrroomba.controller = remote_controllers[0];
    brrroomba.role = ROLE_USER;

	brrroomba.calibration.out_of_track_color.min_value = 454;
	brrroomba.calibration.out_of_track_color.max_value = 723;   // 792

	brrroomba.calibration.race_track_color.min_value = 2730;
	brrroomba.calibration.race_track_color.max_value = 2788;

	// black
	//gimmicks[0].color.min_value = 848;
	//gimmicks[0].color.max_value = 955;

	// alu - acceleration lane
	gimmicks[0].color.min_value = 2866;
	gimmicks[0].color.max_value = 3531;
	gimmicks[0].length = 550;

	// alu - red shell
	gimmicks[1].color.min_value = 2866;
	gimmicks[1].color.max_value = 3531;
	gimmicks[1].length = 300;

	// low battery
	brrroomba.calibration.race_track_color.min_value = 2670;
	brrroomba.calibration.race_track_color.max_value = 2820;

	// Blue:		2885 - 2921
	// Black:		848  -  955
	// Light Blue:	2897 - 2926
	// Green:		2852 - 2898
	// Orange:		2885 - 2910
	// Lila:		2889 - 2917
	// Red:			2894 - 2918
	// Schweinchenfarbe: 2875 - 2904
	// Alu:			2866 - 3531

	brrroomba.calibration.buffer = 50;
	brrroomba.calibration.gimmick_length_buffer = 50;

	return MENU_CONTINUE;
}

/*********************************************************** Local functions */

/**
 * Selects the IR remote control that should be used to control this BrrRoomba.
 * This function sets ::brrroomba.controller.
 *
 * - Press the OK button on the IR remote to select the remote as controller.
 * - Press ::BTN_OK on the Roomba to disable the IR remote (sets ::brrroomba.controller to ::no_remote).
 * - ::IS_BACK quits this procedure without changing the current remote.
 */
static uint8_t settings_select_ir() {
	set_7seg_string("Press OK");

	query_input[0] = PACKET_BUTTONS;
	query_input[1] = PACKET_IR_OMNI;

	uint8_t i = 0;
	uint8_t num_remotes = sizeof(remote_controllers) / sizeof(remote_controller_t);
	while (true) {
		update_7seg();

		query_list(2);

		btns = query_output[0];
		ir = query_output[1];

		for (i = 0; i < num_remotes; ++i) {
			if (ir == remote_controllers[i].ok) {
				brrroomba.controller = remote_controllers[i];
				set_7seg_dec(i);
				ROOMBA_RELEASE_OK();
				return MENU_CONTINUE;
			}
		}

		if (IS_BACK(btns, ir)) {
			ROOMBA_RELEASE_BACK();
			break;
		} else if (IS_OK(btns, ir)) {
			ROOMBA_RELEASE_OK();
			brrroomba.controller = no_remote;
			break;
		}
	}

	return MENU_CONTINUE;
}

/**
 * Helper function for ::settings_role that displays strings instead of role IDs.
 */
static void role_display_fn(int32_t role_id) {
	switch(role_id) {
		case ROLE_USER:
			set_7seg_string("User");
			break;
		case ROLE_CHUCK_NORRIS:
			set_7seg_string("Chuck Norris");
			break;
		default:
			set_7seg_dec(role_id);
			break;
	}
}

/**
 * Sets the Roombas role, stored in ::brrroomba.role.
 * Delegates to ::change_value().
 */
static uint8_t settings_role() {
	brrroomba.role = change_value(brrroomba.role, -1, ROLE_USER, ROLE_CHUCK_NORRIS, false, &role_display_fn);
	return MENU_CONTINUE;
}

/**
 * Tests the IR command mapping stored in IR_DATA.
 * Displays the mapped command for each received IR value.
 * @return ::MENU_CONTINUE
 */
static uint8_t settings_test_ir() {
	query_input[0] = PACKET_BUTTONS;
	query_input[1] = PACKET_IR_OMNI;
	while (true) {
		query_list(2);

		btns = query_output[0];
		ir = query_output[1];

		if (ir == IR_DATA.down) {
			set_7seg_string("Down");
		} else if (ir == IR_DATA.left) {
			set_7seg_string("Left");
		} else if (ir == IR_DATA.ok) {
			set_7seg_string("Ok");
		} else if (ir == IR_DATA.right) {
			set_7seg_string("Right");
		} else if (ir == IR_DATA.stop) {
			set_7seg_string("Stop");
		} else if (ir == IR_DATA.up) {
			set_7seg_string("Up");
		} else if (ir) {
			set_7seg_dec(ir);
		} else {
			set_7seg_string("----");
		}

		// can return only with physical buttons
		if (btns & (BTN_OK | BTN_BACK)) {
			ROOMBA_RELEASE_OK();
			ROOMBA_RELEASE_BACK();
			break;
		}
	}

	return MENU_CONTINUE;
}

/**
 * Tests the received buttons.
 * This test is used to confirm, that an IR remote does not cause any physical button presses.
 * Displays the mapped command for each received button value.
 *
 * Press any bumper or activate the wheel drop to leave this function.
 *
 * @return ::MENU_CONTINUE
 */
static uint8_t settings_test_buttons() {
	volatile uint8_t bumper = 0x00;

	query_input[0] = PACKET_BUTTONS;
	query_input[1] = PACKET_BUMPER;
	query_input[2] = PACKET_IR_OMNI;
	while (true) {
		query_list(3);

		btns = query_output[0];
		bumper = query_output[1];
		ir = query_output[2];

		if (BTN_DOWN & btns) {
			set_7seg_string("Down");
		} else if (BTN_OK & btns) {
			set_7seg_string("Ok");
		} else if (BTN_BACK & btns) {
			set_7seg_string("Stop");
		} else if (BTN_UP & btns) {
			set_7seg_string("Up");
		} else if (btns) {
			set_7seg_dec(btns);
		} else {
			set_7seg_string("----");
		}

		if (bumper) {
			break;
		}
	}

	return MENU_CONTINUE;
}
