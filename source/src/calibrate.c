/****************************************************************** Includes */

#include <calibrate.h>
#include <querylist.h>
#include <segment7.h>

#include <board.h>
#include <led.h>
#include <pio.h>
#include <roomba.h>
#include <tools.h>
#include <uart.h>
#include <roombatools.h>
#include <brrroomba.h>
#include <menu-roomba.h>
#include <settings.h>

/******************************************************************* Defines */

/**
 * Calibration Data used by the BrrRoomba.
 */
#define DATA						brrroomba.calibration

/**
 * The real angle in degree, the Roomba turns in ::calibrate_angle_auto().
 */
#define CALIBRATE_ANGLE				360

/**
 * The real distance in millimeter, the Roomba drives in ::calibrate_distance_auto().
 */
#define CALIBRATE_DISTANCE			1000

/**
 * The velocity used when calibrating colors.
 */
#define CALIBRATE_COLOR_VELOCITY	80

/******************************************************* Function prototypes */

static uint8_t calibrate_angle_auto(int16_t);
static uint8_t calibrate_distance_auto(int16_t);
static void calibrate_color(volatile color_range_t*);

static uint8_t menu_calibrate_distance();
static uint8_t menu_calibrate_angle();
static uint8_t calibrate_color_buffer();
static uint8_t calibrate_length_buffer();
static uint8_t calibrate_color_floor();
static uint8_t calibrate_color_race_track();
static uint8_t calibrate_color_redshell();
static uint8_t calibrate_color_acceleration_lane();
static uint8_t calibrate_length_acceleration_lane();
static uint8_t calibrate_length_redshell();

/************************************************************** Global const */

/********************************************************** Global variables */

/**
 * Submenu for calibrating colors.
 */
static menu_t calibration_color = {
	3,
	{
		{
			"Race Track",
			&calibrate_color_race_track,
			NULL,
		},
		{
			"Floor",
			&calibrate_color_floor,
			NULL,
		},
		{
			"Buffer",
			&calibrate_color_buffer,
			NULL,
		},
	}
};

/**
 * Submenu for calibrating gimmicks.
 */
static menu_t gimmicks_color_length = {
	5,
	{
		{
			"C-Acceleration Lane",
			&calibrate_color_acceleration_lane,
			NULL,
		},
		{
			"L-Acceleration Lane",
			&calibrate_length_acceleration_lane,
			NULL,
		},
		{
			"C-Red Shell",
			&calibrate_color_redshell,
			NULL,
		},
		{
			"L-Red Shell",
			&calibrate_length_redshell,
			NULL,
		},
		{
			"Buffer",
			&calibrate_length_buffer,
			NULL,
		},
	}
};

/**
 * The calibration menu.
 */
menu_t calibration_menu = {
	4,
	{
		{
			"Angle",
			&menu_calibrate_angle,
			NULL
		},
		{
			"Distance",
			&menu_calibrate_distance,
			NULL
		},
		{
			"Color",
			NULL,
			&calibration_color
		},
		{
			"Gimmicks",
			NULL,
			&gimmicks_color_length,
		},
	}
};

/*************************************************************** Local const */

/*********************************************************** Local variables */

volatile static uint8_t btns = 0;		//!< Value for ::PACKET_BUTTONS.
volatile static uint8_t ir = 0;		//!< Value for ::PACKET_IR_OMNI.
volatile static uint16_t cliff_front_left = 0x00;	//!< Value for ::PACKET_CLIFF_FRONT_LEFT.

/******************************************************************** Macros */

/********************************************************** Global functions */

/**
 * Calibrates the angle automagically.
 * The Roomba turns until its front left cliff sensor detects something.
 * Then the real calibration is started. It turns around until it detects something, again.
 * Auto Calibration does not work with the Roomba placed on a straight line.
 * The Roomba must be placed on the end of the marker on the floor.
 *
 * @param velocity The velocity used while calibrating.
 */
static uint8_t calibrate_angle_auto(int16_t velocity) {
	// turn until the front left sensor sees something.
	// then start the calibration.
	drive(velocity, 1);
	do {
		cliff_front_left = query_value(PACKET_CLIFF_FRONT_LEFT);
	} while (!ON_TRACK(cliff_front_left));
	stop();
	msleep(500);

	// reset angle to 0 on roomba
	query_value(PACKET_ANGLE);

	// start auto calibration.
	// turn until the front left sensor sees something.
	drive(velocity, 1);
	msleep(200);
	do {
		cliff_front_left = query_value(PACKET_CLIFF_FRONT_LEFT);
	} while (!ON_TRACK(cliff_front_left));
	stop();

	DATA.raw_angle = query_value(PACKET_ANGLE);
	DATA.raw_angle = ABS(DATA.raw_angle);

	set_7seg_dec(DATA.raw_angle);
	ROOMBA_WAIT_OK();
	return MENU_CONTINUE;
}

/**
 * Calibrates the distance automagically.
 * The Roomba drives until its front left cliff sensor detects something.
 * Then the real calibration is started. It drives until it detects something, again.
 * There must be two markers/lines on the floor with a distance between them of exactly 1 meter.
 *
 * @param velocity The velocity used while calibrating.
 */
static uint8_t calibrate_distance_auto(int16_t velocity) {
	// turn until the front left sensor sees something.
	// then start the calibration.
	drive(velocity, 0);
	do {
		cliff_front_left = query_value(PACKET_CLIFF_FRONT_LEFT);
	} while (!ON_TRACK(cliff_front_left));
	stop();
	msleep(500);

	// reset distance to 0 on roomba
	query_value(PACKET_DISTANCE);

	// start auto calibration.
	// drive until the front left sensor sees something.
	drive(velocity, 0);
	msleep(1000);
	do {
		cliff_front_left = query_value(PACKET_CLIFF_FRONT_LEFT);
	} while (!ON_TRACK(cliff_front_left));
	stop();

	DATA.raw_distance = query_value(PACKET_DISTANCE);
	DATA.raw_distance = ABS(DATA.raw_distance);

	set_7seg_dec(DATA.raw_distance);
	ROOMBA_WAIT_OK();
	return MENU_CONTINUE;
}

/**
 * Reads values from the front cliff sensor (::PACKET_CLIFF_FRONT_LEFT) and sets the
 * values in the given ::color_range_t.
 *
 * While calibrating, the current value from the sensor is displayed on the 7 segment.
 * Once the values look ok, press ::BTN_UP or ::IR_DATA.up to start the calibration.
 * As long as this is pressed, the sensors values are used to set the values in color_wanted.
 * To get the best results, move the Roomba, while pressing ::BTN_UP or ::IR_DATA.up.
 * The Roomba will drive straight while pressing ::BTN_DOWN or ::IR_DATA.down.
 *
 * Press ::BTN_OK or ::IR_DATA.ok to finish the calibration process.
 * Once finished, ::BTN_UP or ::IR_DATA.up will display the calibrated max value
 * and ::BTN_DOWN or ::IR_DATA.down the min value.
 * ::BTN_OK or ::IR_DATA.ok returns to the program.
 *
 * @param color_wanted A color range that will be used to store the calibrated color.
 */
static void calibrate_color(volatile color_range_t *color_wanted) {
	uint16_t color_temp_left = 0;
	int16_t velocity = 0;
	int16_t prev_velocity = 0;

	color_wanted->min_value = 0xffff;
	color_wanted->max_value = 0x0000;

	while (true) {
		update_7seg();

		// retrieve color from front sensors
		query_input[0] = PACKET_CLIFF_FRONT_LEFT;
		query_input[1] = PACKET_BUTTONS;
		query_input[2] = PACKET_IR_OMNI;

		query_list(3);

		color_temp_left = query_output[0];
		btns 			= query_output[1];
		ir	 			= query_output[2];

		//todo: realtime color displaying
		set_7seg_dec(color_temp_left);
		msleep(10);

		if (IS_OK(btns, ir)) {
			ROOMBA_RELEASE_OK();
			break;
		} else if (IS_UP(btns, ir)) {

			//set min_color_value
			if (color_temp_left < color_wanted->min_value){
				color_wanted->min_value = color_temp_left;
			}

			//set max_color_value
			if (color_temp_left > color_wanted->max_value){
				color_wanted->max_value = color_temp_left;
			}
		}
		
		if (IS_DOWN(btns, ir)) {
			velocity = CALIBRATE_COLOR_VELOCITY;
		} else {
			velocity = 0;
		}
		
		if (velocity != prev_velocity) {
			prev_velocity = velocity;
			drive(velocity, 0);
		}
	}
	
	stop();

	//display values after BTN_OK pressed
	while(true) {
		update_7seg();

		query_input[0] = PACKET_BUTTONS;
		query_input[1] = PACKET_IR_OMNI;

		query_list(2);

		btns			= query_output[0];
		ir	 			= query_output[1];

		if (IS_OK(btns, ir)) {
			ROOMBA_RELEASE_OK();
			break;
		} else if (IS_UP(btns, ir)) {
			set_7seg_dec(color_wanted->max_value);
			ROOMBA_RELEASE_UP();
		} else if (IS_DOWN(btns, ir)) {
			set_7seg_dec(color_wanted->min_value);
			ROOMBA_RELEASE_DOWN();
		}
	}
}

/**
 * Transforms the given angle into a new angle based on a previous calibration.
 * The returned value can be compared to values retrieved from the Roomba using ::PACKET_ANGLE.
 * @param angle The angle in degree.
 * @return The given angle transformed to a value, that represents the same angle for the Roomba.
 */
inline int32_t get_angle(int32_t angle) {
	return (angle * DATA.raw_angle) / CALIBRATE_ANGLE;
}

/**
 * Transforms the given distance into a new distance based on a previous calibration.
 * The returned value can be compared to values retrieved from the Roomba using ::PACKET_DISTANCE.
 * @param distance The distance in millimeter.
 * @return The given distance transformed to a value, that represents the same distance for the Roomba.
 */
inline int32_t get_distance(int32_t distance) {
	return (distance * DATA.raw_distance) / CALIBRATE_DISTANCE;
}

/*********************************************************** Local functions */

/**
 * Calibrates the color for the race track (::DATA.race_track_color).
 */
static uint8_t calibrate_color_race_track() {
	calibrate_color(&(DATA.race_track_color));
	return MENU_CONTINUE;
}

/**
 * Calibrates the color for the floor (::DATA.out_of_track_color).
 */
static uint8_t calibrate_color_floor() {
	calibrate_color(&(DATA.out_of_track_color));
	return MENU_CONTINUE;
}

/**
 * Calibrates the color of the gimmick Acceleration Lane.
 */
static uint8_t calibrate_color_acceleration_lane() {
#define GIMMICK_ACCELERATION_LANE_INDEX	0
	calibrate_color(&(gimmicks[GIMMICK_ACCELERATION_LANE_INDEX].color));
	return MENU_CONTINUE;
}


/**
 * Calibrates the color of the gimmick Red Shell.
 */
static uint8_t calibrate_color_redshell() {
#define GIMMICK_REDSHELL_INDEX	1
	calibrate_color(&(gimmicks[GIMMICK_REDSHELL_INDEX].color));
	return MENU_CONTINUE;
}

/**
 * Sets the value ::DATA.buffer.
 * Delegates functionality to ::change_value().
 */
static uint8_t calibrate_color_buffer() {
	DATA.buffer = change_value(DATA.buffer, 5, 0, 200, true, NULL);
	return MENU_CONTINUE;
}

/**
 * Sets the value ::DATA.gimmick_length_buffer.
 * Delegates functionality to ::change_value().
 */
static uint8_t calibrate_length_buffer() {
	DATA.gimmick_length_buffer = change_value(DATA.gimmick_length_buffer, 5, 0, 200, true, NULL);
	return MENU_CONTINUE;
}

/**
 * Calibrates the angle with a velocity of 500.
 */
static uint8_t menu_calibrate_angle() {
	calibrate_angle_auto(500);
	return MENU_CONTINUE;
}

/**
 * Calibrates the distance with a velocity of 300.
 */
static uint8_t menu_calibrate_distance() {
	calibrate_distance_auto(300);
	return MENU_CONTINUE;
}

/**
 * Calibrates the length of the acceleration lane gimmick.
 */
static uint8_t calibrate_length_acceleration_lane() {
	gimmicks[0].length = change_value(gimmicks[0].length, 10, 0, 2000, true, NULL);
	return MENU_CONTINUE;
}

/**
 * Calibrates the length of the red shell gimmick.
 */
static uint8_t calibrate_length_redshell() {
	gimmicks[1].length = change_value(gimmicks[1].length, 10, 0, 2000, true, NULL);
	return MENU_CONTINUE;
}
