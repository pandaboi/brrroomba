/****************************************************************** Includes */

#include <stdint.h>
#include <game.h>
#include <querylist.h>
#include <segment7.h>
#include <roomba.h>
#include <roombatools.h>
#include <tools.h>
#include <statemachine.h>
#include <calibrate.h>
#include <brrroomba.h>
#include <ir.h>
#include <settings.h>
#include <menu-roomba.h>
#include <sound.h>

/******************************************************************* Defines */

/// Global Game Data
#define DATA	brrroomba.game

/**
 * The maximum velocity, the Roomba can/should drive.
 */
#define VELOCITY_MAX    1000

/**
 * The maximum velocity, the Roomba can drive in normal mode, i.e. without any applied gimmicks.
 * A velocity of 300 equals 0.38 m/s or 1.38 km/h.
 * The Roomba drives 4 meter within 10.4 seconds.
 */
#define VELOCITY_NORMAL_MODE_MAX	300

/**
 * The maximum velocity, the Roomba can drive with the Gimmick ::GIMMICK_ACCELERATION_LANE applied.
 * A velocity of 1000 equals 0.57 m/s or 2.06 km/h.
 * The Roomba drives 4 meter within 7.0 seconds.
 */
#define VELOCITY_SPEED_BONUS_MAX	1000

/**
 * The maximum velocity, the Roomba can drive with the Gimmick ::GIMMICK_SWAMP applied.
 */
#define VELOCITY_SWAMP_MAX	100

/**
 * The maximum velocity, the Roomba can/should drive in manual driving mode.
 */
#define VELOCITY_MANUAL_MAX		(VELOCITY_NORMAL_MODE_MAX >> 0)

/**
 * The increment of the velocity using the remote.
 * The slower the Roomba, the greater the increase in velocity.
 */
#define VELOCITY_STEP_ACCELERATE	(1 + ((DATA.velocity_max - DATA.velocity) >> 6))

/**
 * The decrement of the velocity using the remote.
 * The faster the Roomba, the greater the decrease in velocity.
 */
#define VELOCITY_STEP_DECELERATE	(1 + ((DATA.velocity_max + DATA.velocity) >> 6))

/**
 * Used in ::ST_MANUAL_DRIVE.
 * If the velocity is between -VELOCITY_BUFFER and VELOCITY_BUFFER, the Roomba will turn in place.
 */
#define VELOCITY_BUFFER			50

/**
 * Used in ::ST_MANUAL_DRIVE.
 * When turning in place (as detected with ::VELOCITY_BUFFER) this velocity is used.
 */
#define VELOCITY_TURN_IN_PLACE	200

/**
 * The minimum radius that is calculated by the ::folow_line_algorithm() function.
 * Setting this value too low results in abrupt turns. A value of 30 seems to be ok.
 */
#define MIN_RADIUS	30

/**
 * The maximum radius, the Roomba can drive.
 */
#define MAX_RADIUS 2000

/**
 * The radius, the Roomba drives when the user wants to take a turn in ::ST_FOLLOWLINE.
 */
#define BRANCH_RADIUS   300

/**
 * The radius, the Roomba drives during the state ::ST_MANUAL_DRIVE.
 */
#define MANUAL_RADIUS   150

/**
 * The number of ticks, the bumper must be pressed to stop the Roomba in ::ST_MANUAL_DRIVE.
 * In the manual driving state, we should be a bit more forgiving than in ::ST_FOLLOWLINE as it is
 * quite easy to slightly bump against a wall.
 */
#define MANUAL_BUMPER_TICKS	20

/**
 * The radius, the Roomba drives during the state ::ST_SENSOR_SWITCH.
 */
#define SWITCH_RADIUS	70

/**
 * Both front sensors must be on the floor for this number of ticks to
 * activate the ::ST_MANUAL_DRIVE state.
 */
#define TICKS_TO_MANUAL_DRIVE	100

/**
 * Determines if the Roomba is currently driving with the left front cliff sensor (::cliff_front_left).
 */
#define DRIVING_LEFT	(cliff_track == &cliff_front_left)

/**
 * Determines if the Roomba is currently driving with the right front cliff sensor (::cliff_front_right).
 */
#define DRIVING_RIGHT	(cliff_track == &cliff_front_right)

/**
 * The IR command that will be sent as Redshell.
 * This is not required to be tied to a Roomba.
 */
#define IR_CMD_REDSHELL		142

/******************************************************* Function prototypes */

// states
static uint8_t st_start();
static uint8_t st_stop();
static uint8_t st_followline();
static uint8_t st_sensor_switch();
static uint8_t st_manual_driving();
static uint8_t st_banana();

// others
static inline void led_go_nuts();
static inline void led_go_off();
static inline void follow_line_algorithm();
static inline void update_roomba_with_ir();
static inline void handle_global();

// Utility functions
static inline void update_drive();
static inline uint8_t check_bumper();
static inline uint8_t check_stop();
static inline uint8_t check_manual_drive();
static inline uint8_t check_redshell();
static inline void check_gimmicks();
static inline void display_gimmick();

/************************************************************** Global const */

/********************************************************** Global variables */


/**
 * All possible gimmicks. There may be a lot of them.
 */
enum gimmick_t {
	/**
	 * No gimmick.
	 */
	GIMMICK_NONE,

	/**
	 * Acceleration lane gimmick.
	 * Ramp up the maximum velocity to ::VELOCITY_SPEED_BONUS_MAX for the next x ticks.
	 */
	GIMMICK_ACCELERATION_LANE,

	/**
	 * Deceleration/Swamp gimmick.
	 * Decrease the maximum velocity to ::VELOCITY_SWAMP_MAX for the next x ticks.
	 */
	GIMMICK_SWAMP,

	/**
	 * Red shell gimmick.
	 * Allows the BrrRoomba to shoot others with its IR cannon.
	 */
	GIMMICK_REDSHELL,
};

/**
 * Definition of all implemented gimmicks.
 */
gimmick_t gimmicks[] = {
		{
				GIMMICK_ACCELERATION_LANE,
				{0,0},
				0,
				&gimmick_pickup,
				&use_gimmick_sound,
				2000,
		},
		{
				GIMMICK_REDSHELL,
				{0,0},
				0,
				&gimmick_pickup,
				&use_gimmick_sound,
				50,
		},
};

/*************************************************************** Local const */

/*********************************************************** Local variables */

/**
 * All possible game states.
 */
enum state_t
{
	ST_START,
    ST_FOLLOWLINE,
    ST_MANUAL_DRIVE,
    ST_SENSOR_SWITCH,
    ST_BANANA,
    ST_STOP,
    ST_EXIT,
};




volatile static uint8_t btns = 0;
volatile static uint8_t ir = 0;
volatile static uint8_t bumper = 0;

volatile static uint16_t cliff_left = 0;
volatile static uint16_t cliff_right = 0;
volatile static uint16_t cliff_front_left = 0;
volatile static uint16_t cliff_front_right = 0;

volatile static uint16_t *cliff_track = &cliff_front_left;
volatile static uint16_t *cliff_floor = &cliff_front_right;

/******************************************************************** Macros */

/**
 * Set the value ::DATA.velocity to the given value, with respect to ::DATA.velocity_max.
 */
#define SET_VELOCITY(value)     DATA.velocity = (ABS(value) > DATA.velocity_max ? SIGN(value) * DATA.velocity_max : value)

/********************************************************** Global functions */

static transition_t transitions[] =
{
	{ ST_START,					&st_start					},
	{ ST_FOLLOWLINE,			&st_followline				},
	{ ST_MANUAL_DRIVE,			&st_manual_driving			},
	{ ST_SENSOR_SWITCH,			&st_sensor_switch			},
	{ ST_BANANA,        		&st_banana          		},
	{ ST_STOP,					&st_stop					},
};

/**
 * State machine for the gameplay.
 */
uint8_t game_main() {

    DATA.velocity_max = VELOCITY_NORMAL_MODE_MAX;
    DATA.velocity = 100;

    play_song(&start_sound);

    sm_start(
        ST_START, ST_EXIT,
        transitions, sizeof(transitions) / sizeof(transition_t*)
    );

    return MENU_CONTINUE;
}

uint8_t manual_main() {
    DATA.velocity_max = VELOCITY_NORMAL_MODE_MAX;
    DATA.velocity = 100;

    sm_start(
        ST_MANUAL_DRIVE, ST_EXIT,
        transitions, sizeof(transitions) / sizeof(transition_t*)
    );

    return MENU_CONTINUE;
}


/*********************************************************** Local functions */

/**
 * Implementation of the line following algorithm.
 * This functions looks at the variables ::cliff_track and ::cliff_floor
 * and calculates a new radius according to their values. The new radius
 * is written to ::DATA.radius.
 *
 * How it works: ::cliff_track should stay on the line.
 * Depending on the actual sensor of ::cliff_track the checks for turning left/right
 * are swapped. The following assumes ::cliff_front_left as ::cliff_track.
 *
 * - ::cliff_track is on the line: move straight
 *     ::TRACK_LOW <= ::cliff_track <= ::TRACK_HIGH
 *
 * - ::cliff_track is right of the line: turn left
 *     ::FLOOR_LOW <= ::cliff_track <  ::TRACK_LOW
 *     ::FLOOR_LOW <= ::cliff_floor <= ::FLOOR_HIGH
 *
 * - ::cliff_track is left of the line: turn right
 *     ::FLOOR_LOW  <= ::cliff_track < ::TRACK_LOW
 *     ::FLOOR_HIGH <  ::cliff_floor < ::TRACK_HIGH
 *
 * The higher the difference from the track, the smaller the radius will be.
 *
 * Accessed variables:
 * - ::cliff_track
 * - ::cliff_floor
 * - ::DATA.radius
 */
static inline void follow_line_algorithm() {
	static int16_t smooth_radius = 0;
	static const uint8_t smooth_count = 10;

	int16_t diff = 0;
	int8_t sign = 1;

	if (DRIVING_RIGHT) {
		sign = -1;
	}

	if (ON_TRACK(*cliff_track))
	{
		sleds(1<<0,0x00);
		DATA.radius = 0;
		smooth_radius = 0;
	}
	else if ((ON_FLOOR(*cliff_track) || BETWEEN_TRACK_FLOOR(*cliff_track)) && ON_FLOOR(*cliff_floor))
	{
		sleds(1<<1,0x00);
		// turn left
		diff = ABS(TRACK_AVG - (*cliff_track));
		DATA.radius = sign * (MAX_RADIUS + MIN_RADIUS - (diff * MAX_RADIUS / ABS(TRACK_AVG - FLOOR_MIN)));
	}
	else if ((BETWEEN_TRACK_FLOOR(*cliff_track) || ON_FLOOR(*cliff_track)) && (ON_TRACK(*cliff_floor) || BETWEEN_TRACK_FLOOR(*cliff_floor)))
	{
		sleds(1<<2,0x00);
		// turn right
		diff = ABS(TRACK_AVG - (*cliff_track));
		DATA.radius = -sign * (MAX_RADIUS + MIN_RADIUS - (diff * MAX_RADIUS / ABS(TRACK_AVG - FLOOR_MIN)));
	}

	smooth_radius = (int16_t)((smooth_radius * smooth_count + DATA.radius) / (smooth_count + 1));
	DATA.radius = smooth_radius;
}

/**
 * Updates the Roomba based on the currently received IR command.
 * This function also handles the lifespan of gimmicks.
 *
 * Accessed variables:
 * - ::ir
 * - ::DATA.velocity
 * - ::DATA.gimmick
 */
static inline void update_roomba_with_ir() {
	static uint16_t gimmick_ticks = 0;

	//roomba got some gimmick
	if (gimmick_ticks > 0) {
		gimmick_ticks--;

		// Revert gimmick
		if (gimmick_ticks == 0) {
			switch(DATA.gimmick->id) {
				case GIMMICK_ACCELERATION_LANE:
					DATA.velocity_max = VELOCITY_NORMAL_MODE_MAX;
					//velocity down again
					DATA.velocity = DATA.velocity < VELOCITY_NORMAL_MODE_MAX ? DATA.velocity : VELOCITY_NORMAL_MODE_MAX;//(DATA.velocity * VELOCITY_NORMAL_MODE_MAX)/ VELOCITY_SPEED_BONUS_MAX;

					uart_write_byte(CMD_MOTORS);
					uart_write_byte(0x00);
					break;
				case GIMMICK_REDSHELL:
					ir_sender_off();
					DATA.bulletproof = 0;
					break;
			}

			DATA.gimmick = NULL;
		}
	}

	int16_t tmp = 0;
	if (IR_DATA.up == ir)
	{
		tmp = DATA.velocity + VELOCITY_STEP_ACCELERATE;
		SET_VELOCITY(tmp);
	}
	else if (IR_DATA.down == ir)
	{
		tmp = DATA.velocity - VELOCITY_STEP_DECELERATE;
		SET_VELOCITY(tmp);
	}
	else if (IR_DATA.ok == ir && gimmick_ticks == 0 && DATA.gimmick != NULL)
	{
		play_song(DATA.gimmick->sound_activate);
		gimmick_ticks = DATA.gimmick->duration;

		// Apply gimmick only if no other gimmick is currently in use.
		switch(DATA.gimmick->id) {
			case GIMMICK_ACCELERATION_LANE:
				DATA.velocity_max = VELOCITY_SPEED_BONUS_MAX;
				DATA.velocity += VELOCITY_SPEED_BONUS_MAX - VELOCITY_NORMAL_MODE_MAX;
				uart_write_byte(CMD_PWM_MOTORS);
				uart_write_byte(0x00);
				uart_write_byte(0x00);
				uart_write_byte(32);
				break;
			case GIMMICK_REDSHELL:
				DATA.bulletproof = 1;
				ir_sender_set(IR_CMD_REDSHELL, IR_CMD_REDSHELL, IR_CMD_REDSHELL, IR_CMD_REDSHELL);
				ir_sender_on();
				break;
		}
	}
}

/**
 * Uses the Clean LED on the Roomba to display which gimmick is currently carried by the Roomba.
 * - No gimmick picked up: LED is off
 * - ::gimmicks[0] (acceleration lane): green LED
 * - ::gimmicks[1] (red shell): red LED
 */
static inline void display_gimmick() {
	static gimmick_t *last_gimmick = NULL;
	static uint8_t color = 0;
	static uint8_t intensity = 0;

	if (last_gimmick != DATA.gimmick) {
		last_gimmick = DATA.gimmick;

		if (last_gimmick == NULL) {
			intensity = 0x00;
		} else {
			uint8_t i = 0;
			for (i = 0; i < sizeof(gimmicks) / sizeof(gimmick_t); ++i) {
				if (&(gimmicks[i]) == last_gimmick) {
					color = i * 0xff;
					intensity = 0xff;
					break;
				}
			}
		}
	}

	leds(0x00, color, intensity);
}

/**
 * Perform global actions, like updating the 7 segment display.
 */
static inline void handle_global()
{
    update_7seg();
    update_longsong();
}

/**
 * Let the roomba leds go nuts
 */
static inline void led_go_nuts(){
    leds(0xf,0xff,0xff);
    sleds(0xff,0xff);
}

/**
 * roomba leds off
 */
static inline void led_go_off(){
    leds(0x0,0x00,0x00);
    sleds(0x00,0x00);
}


/******************************************************************** States */

/**
 * State: switch the driving sensors.
 * This state switches the pointers ::cliff_track and ::cliff_floor and waits
 * until the Roomba is driving correctly with the changed sensors.
 *
 * @return
 * - ::ST_PREVIOUS once the switch is complete
 * - ::ST_MANUAL_DRIVE
 * - ::ST_STOP
 * - ::ST_BANANA
 */
static uint8_t st_sensor_switch() {
	// switch sensors and turn to get the appropriate sensor on the track.
	if (DRIVING_LEFT) {
		cliff_track = &cliff_front_right;
		cliff_floor = &cliff_front_left;

		DATA.radius = SWITCH_RADIUS;
	} else {
		cliff_track = &cliff_front_left;
		cliff_floor = &cliff_front_right;

		DATA.radius = -SWITCH_RADIUS;
	}

	// wait until the sensor is on track.
	while (true) {
		handle_global();

        query_input[0] = PACKET_CLIFF_FRONT_LEFT;
        query_input[1] = PACKET_CLIFF_FRONT_RIGHT;
        query_input[2] = PACKET_BUTTONS;
        query_input[3] = PACKET_IR_OMNI;
        query_input[4] = PACKET_BUMPER;
        query_input[5] = PACKET_CLIFF_LEFT;
        query_input[6] = PACKET_CLIFF_RIGHT;

        query_list(7);

		cliff_front_left	= query_output[0];
		cliff_front_right	= query_output[1];
        btns				= query_output[2];
        ir					= query_output[3];
        bumper				= query_output[4];
        cliff_left    		= query_output[5];
        cliff_right   		= query_output[6];

		// Detect gimmicks
		check_gimmicks();

		if (ON_TRACK(*cliff_track)) {
			return ST_PREVIOUS;
		}

		if (check_manual_drive()) {
			return ST_MANUAL_DRIVE;
		}
        if (check_stop()) {
            return ST_STOP;
        }
		if (check_bumper() || check_redshell()) {
			return ST_BANANA;
		}

		update_roomba_with_ir();
		update_drive();
	}

	return ST_STOP;
}

/**
 * Stop Menu handler function: change state to ::ST_MANUAL_DRIVE.
 */
static uint8_t stop_menu_manual() {
	return ST_MANUAL_DRIVE;
}

/**
 * Stop Menu handler function: change state to ::ST_EXIT.
 */
static uint8_t stop_menu_exit() {
	return ST_EXIT;
}

/**
 * State: game pause.
 *
 * @return
 * - ::ST_PREVIOUS on Resume
 * - ::ST_MANUAL_DRIVE on Manual
 * - ::ST_EXIT on Exit
 */
static uint8_t st_stop()
{
	static menu_t stop_menu = {
		3,
		{
			{
				"Resume",
				&menu_exit,
				NULL,
			},
			{
				"Manual",
				&stop_menu_manual,
				NULL,
			},
			{
				"Exit",
				&stop_menu_exit,
				NULL,
			},
		}
	};

	uint8_t state = display_menu(&stop_menu, 1, 1);
	if (!IS_MENU_CODE(state)) {
		return state;
	} else {
		return ST_PREVIOUS;
	}
}

/**
 * Waits until the start signal and continues with ::ST_FOLLOWLINE.
 * Additionally sets the driving sensor according to the currently
 * received values on the front cliff sensors.
 *
 * @return
 * - ::ST_FOLLOWLINE
 */
static uint8_t st_start() {

	query_input[0] = PACKET_CLIFF_FRONT_LEFT;
	query_input[1] = PACKET_CLIFF_FRONT_RIGHT;

	query_list(2);

	cliff_front_left    = query_output[0];
	cliff_front_right   = query_output[1];

	// Check which sensor is currently closest to the track and set this
	// one as the ::cliff_track sensor.
	if (ABS(TRACK_AVG - cliff_front_left) <= ABS(TRACK_AVG - cliff_front_right)) {
		cliff_track = &cliff_front_left;
		cliff_floor = &cliff_front_right;
	} else {
		cliff_track = &cliff_front_right;
		cliff_floor = &cliff_front_left;
	}

	return ST_FOLLOWLINE;
}

/**
 * State: Follows a white line on the ground.
 *
 * @return
 * - ::ST_MANUAL_DRIVE
 * - ::ST_STOP
 * - ::ST_SENSOR_SWITCH
 * - ::ST_BANANA
 */
static uint8_t st_followline()
{
    while (true)
    {
        handle_global();

        query_input[0] = PACKET_CLIFF_FRONT_LEFT;
        query_input[1] = PACKET_CLIFF_FRONT_RIGHT;
        query_input[2] = PACKET_BUTTONS;
        query_input[3] = PACKET_IR_OMNI;
        query_input[4] = PACKET_BUMPER;
        query_input[5] = PACKET_CLIFF_LEFT;
        query_input[6] = PACKET_CLIFF_RIGHT;

        query_list(7);

        cliff_front_left    = query_output[0];
        cliff_front_right   = query_output[1];
        btns                = query_output[2];
        ir                  = query_output[3];
        bumper              = query_output[4];
        cliff_left    		= query_output[5];
        cliff_right   		= query_output[6];

		// Manual drive state-transission
		if (check_manual_drive()) {
			return ST_MANUAL_DRIVE;
		}

		// Detect gimmicks
		check_gimmicks();

		// Display front cliff sensor
		set_7seg_dec(*cliff_track);

        if (check_stop()) {
            return ST_STOP;
        }

		if (ir == IR_DATA.left) {
			if (!DRIVING_RIGHT) {
				return ST_SENSOR_SWITCH;
			}

			DATA.radius = BRANCH_RADIUS;
		} else if (ir == IR_DATA.right) {
			if (!DRIVING_LEFT) {
				return ST_SENSOR_SWITCH;
			}

			DATA.radius = -BRANCH_RADIUS;
		} else {
			follow_line_algorithm();
		}

        // IR commands
		update_roomba_with_ir();

		if (check_bumper() || check_redshell()) {
			return ST_BANANA;
		}

		update_drive();
    }

    stop();
    return ST_FOLLOWLINE;
}

/**
 * State: manual controlling with ir remote control.
 *
 * If race track is recognized under the ::cliff_track sensor the Clean LED changes its color from red to green.
 * Once the LED is green, press ::IR_DATA.ok to continue with the line following algorithm.
 *
 * The Roomba recognizes the signals of his remote control (::IR_DATA) and hopefully follows the instructions:
 *		- ::IR_DATA.up		-> increment velocity
 *		- ::IR_DATA.down	-> decrement velocity
 *		- ::IR_DATA.left	-> turn left
 *		- ::IR_DATA.right	-> turn right
 *		- ::IR_DATA.stop	-> jump to ::ST_STOP
 *		- ::IR_DATA.ok		-> jump to ::ST_FOLLOWLINE
 */
static uint8_t st_manual_driving()
{
	/// Counts the ticks, the bumper was pressed. After ::MANUAL_BUMPER_TICKS, the Roomba is stopped.
	static uint8_t bumper_ticks = 0;

	set_7seg_string("Man");

    //stop the roomba before doing further tasks
    DATA.velocity = 0;

    // if true, set DATA.velocity to 0 if left or right are no longer pressed.
    uint8_t reset_velocity = 0;

    while (true)
    {
		handle_global();

        //QUERY LIST
        query_input[0] = PACKET_BUTTONS;
        query_input[1] = PACKET_IR_OMNI;
        query_input[2] = PACKET_CLIFF_FRONT_LEFT;
        query_input[3] = PACKET_CLIFF_FRONT_RIGHT;
        query_input[4] = PACKET_BUMPER;

        query_list(5);

        btns = query_output[0];
        ir = query_output[1];
        cliff_front_left = query_output[2];
        cliff_front_right = query_output[3];
        bumper = query_output[4];

        // Red LED when on the floor; Green LED when above the track.
        if (ON_TRACK(*cliff_track))
        {
        	leds(0x00, 0x00, 0xff);
        }
        else
        {
			leds(0x00, 0xff, 0xff);
        }

        if (check_stop())
        {
        	led_go_off();
            return ST_STOP;
        }

        // manual driving works only with IR
        if(IR_DATA.up == ir || IR_DATA.down == ir)
        {
            DATA.radius =  0;
        }
        else if(IR_DATA.left == ir)
        {
        	if (reset_velocity || BETWEEN(DATA.velocity, -VELOCITY_BUFFER, VELOCITY_BUFFER)) {
        		reset_velocity = 1;
        		SET_VELOCITY(DATA.velocity + VELOCITY_STEP_ACCELERATE);
				DATA.radius = 1;
        	} else {
        		DATA.radius = MANUAL_RADIUS;
        	}
        }
        else if(IR_DATA.right == ir)
        {
        	if (reset_velocity || BETWEEN(DATA.velocity, -VELOCITY_BUFFER, VELOCITY_BUFFER)) {
        		reset_velocity = 1;
        		SET_VELOCITY(DATA.velocity + VELOCITY_STEP_ACCELERATE);
				DATA.radius = -1;
        	} else {
        		DATA.radius = -MANUAL_RADIUS;
        	}
        }
        else if(IR_DATA.ok == ir)
        {
        	// Continue driving when the user is above the track.
        	// If the user presses ok outside the track, the velocity is set to 0.
        	// This can be used as a safety-brake button. Additionally the user should have
        	// a disadvantage, if he tries to drive outside the track.
        	if (!ON_TRACK(*cliff_track) && !ON_TRACK(*cliff_floor)) {
        		DATA.velocity = 0;
        	}
        	DATA.radius = 0;
        	led_go_off();
        	ROOMBA_IR_RELEASE(IR_DATA.ok);
            return ST_FOLLOWLINE;
        }
        else
        {
        	if (reset_velocity) {
        		DATA.velocity = 0;
        		reset_velocity = 0;
        	}

        	DATA.radius = 0;
        }

        // act on bumpers only after ::MANUAL_BUMPER_TICKS
        if (bumper & (BUMPER_LEFT | BUMPER_RIGHT)) {
        	bumper_ticks++;
        	if (bumper_ticks >= MANUAL_BUMPER_TICKS) {
        		bumper_ticks = 0;
        		if (ABS(DATA.radius) != 1) {
        			DATA.velocity = 0;
        		}
        	}
        } else if (bumper_ticks) {
        	bumper_ticks = 0;
        }

        if (check_redshell()) {
        	return ST_BANANA;
        }

        // Update gimmick status. must be done last, as it checks IR_DATA.ok and possibly activates a gimmick.
        // As IR_DATA.ok was checked previously (with a return to ST_FOLLOWLINE), gimmicks cannot be activated herein.
        update_roomba_with_ir();

        update_drive();
    }

    led_go_off();
    return ST_STOP;
}

/**
 * State: do a three-sixty
 * This state does not require any other checks like ::check_stop() or ::check_manual_drive().
 * The user should have no control when hitting the banana.
 *
 * @return
 * - ::ST_PREVIOUS
 * - ::ST_MANUAL_DRIVE
 */
static uint8_t st_banana()
{
    int32_t tmp_radius = 0;
    int32_t three_sixty = get_angle(360);

    //reset
    query_value(PACKET_ANGLE);

    int16_t prev_velocity = DATA.velocity;
    int16_t prev_radius = DATA.radius;

    DATA.velocity = VELOCITY_MAX;
    DATA.radius = -1;
    update_drive();

    while (ABS(tmp_radius) < three_sixty) {
		handle_global();

        led_go_nuts();
        msleep(50);

        led_go_off();
        msleep(10);

        tmp_radius += query_value(PACKET_ANGLE);

        if (check_manual_drive()) {
        	return ST_MANUAL_DRIVE;
        }
    }
    led_go_off();

    DATA.velocity = prev_velocity;
    DATA.radius = prev_radius;
    update_drive();

    return ST_PREVIOUS;
}


/*********************************************************** Utility functions */

/**
 * Let the Roomba drive according to the values ::DATA.velocity and ::DATA.radius.
 * The drive command is only issued, if velocity or radius changed since the last call.
 *
 * Accessed variables:
 * - ::DATA.velocity
 * - ::DATA.radius
 */
static inline void update_drive() {
	// static variables are initialized only once across function calls.
	static int16_t prev_radius = 0;
	static int16_t prev_velocity = 0;

	// drive according to calculated radius
	if (DATA.radius != prev_radius || DATA.velocity != prev_velocity)
	{
		prev_radius = DATA.radius;
		prev_velocity = DATA.velocity;
		drive(DATA.velocity, DATA.radius);
	}
}

/**
 * Checks if the bumpers are pressed and stops the Roomba if they are.
 *
 * Accessed variables:
 * - ::bumper
 *
 * @return Boolean value, indicating, whether a bumper was pressed (::true) or not (::false).
 */
static inline uint8_t check_bumper() {
	// Bumper checking.
	if (bumper & (BUMPER_LEFT | BUMPER_RIGHT)) {
		stop();
		return true;
	}

	return false;
}

/**
 * Checks if the program should be stopped and transission to another state (preferably to ::ST_STOP).
 *
 * Accessed variables:
 * - ::btns
 * - ::ir
 *
 * @return Boolean value, indicating, whether the program should be stopped (::true) or not (::false).
 */
static inline uint8_t check_stop() {
	if ((btns & BTN_CLEAN) || (btns & BTN_DAY) || ir == IR_DATA.stop) {
		stop();
		ROOMBA_BUTTON_RELEASE(BTN_CLEAN);
		ROOMBA_BUTTON_RELEASE(BTN_DAY);
		ROOMBA_IR_RELEASE(IR_DATA.stop);
		return true;
	}

	return false;
}

/**
 * Checks if the Roomba was off the track for more than ::TICKS_TO_MANUAL_DRIVE calls of this function.
 * If so, the Roomba is stopped and this function returns ::true.
 *
 * Accessed variables:
 * - ::cliff_track
 * - ::cliff_floor
 *
 * @return Boolean value, indicating, whether the program should transission to the manual drive state (::true) or not (::false).
 */
static inline uint8_t check_manual_drive() {
	static uint16_t tick_manual = 0;

	if (ON_FLOOR(*cliff_track) && ON_FLOOR(*cliff_floor)) {
		tick_manual++;

		if (tick_manual > TICKS_TO_MANUAL_DRIVE) {
			tick_manual = 0;
			stop();
			return true;
		}
	} else {
		tick_manual = 0;
	}

	return false;
}

/**
 * Check if the Roomba is above a gimmick and pick it up. (Checks only if the Roomba is on track)
 * Gimmicks are only picked up if no other is currently carried in ::DATA.gimmick.
 *
 * Accessed variables:
 * - ::cliff_left
 * - ::cliff_right
 *
 * Note: This function calls `query_list(1)`.
 */
static inline void check_gimmicks() {
	static uint8_t is_on_gimmick = 0;
	static color_range_t* gimmick_color = NULL;
	static uint8_t numbers_of_gimmicks = sizeof(gimmicks) / sizeof(gimmick_t);

	uint8_t prev_is_on_gimmick = is_on_gimmick;

	uint8_t i = 0;
	for (i = 0; i < numbers_of_gimmicks; ++i) {
		if (ON_COLOR(cliff_left, gimmicks[i].color) && ON_FLOOR(cliff_right)) {
			// Maybe this does not work. The color may be changed!
			is_on_gimmick = 1;
			gimmick_color = &(gimmicks[i].color);
			break;
		} else {
			is_on_gimmick = 0;
		}
	}

	if (prev_is_on_gimmick == 0 && is_on_gimmick == 1) {
		// gimmick detected. reset distance.
		query_value(PACKET_DISTANCE);
	} else if (prev_is_on_gimmick == 1 && is_on_gimmick == 0) {
		// end of gimmick. retrieve distance.
		int16_t distance = query_value(PACKET_DISTANCE);
		distance = ABS(distance);
		int16_t min_dist = 0;
		int16_t max_dist = 0;

		for (i = 0; i < numbers_of_gimmicks; ++i) {
			min_dist = get_distance(gimmicks[i].length - brrroomba.calibration.gimmick_length_buffer);
			max_dist = get_distance(gimmicks[i].length + brrroomba.calibration.gimmick_length_buffer);
			if (BETWEEN(distance, min_dist, max_dist) &&
					gimmick_color->max_value == gimmicks[i].color.max_value &&
					gimmick_color->min_value == gimmicks[i].color.min_value) {
				if (DATA.gimmick == NULL) {
					DATA.gimmick = &(gimmicks[i]);
					play_song(gimmicks[i].sound_pickup);
				} else {
					play_song(&gimmick_pickup_failed);
				}

				break;
			}
		}
	}

	display_gimmick();
}

/**
 * Checks if the Roomba is hit by a red shell. This check also includes
 * checking if the Roomba is currently bulletproof (see ::DATA.bulletproof).
 *
 * Accessed variables:
 * - ::ir
 *
 * @return ::true, if he was hit; otherwise ::false.
 */
static inline uint8_t check_redshell() {
	return !DATA.bulletproof && ir == IR_CMD_REDSHELL;
}
