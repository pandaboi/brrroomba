/******************************************************************************

File: main.c

Project:

Description: Programming tools

Author: Pham Van-Lang <gerdes@informatik.uni-augsburg.de>
	Tobias Wallura <gerdes@informatik.uni-augsburg.de>
        Universität Augsburg

Created: 26.04.2011

*******************************************************************************

Modification history:
---------------------

*/


/****************************************************************** Includes */

#include "board.h"
#include "led.h"
#include "pio.h"
#include "roomba.h"
#include "tools.h"
#include "roombatools.h"
#include "uart.h"

#include "sound.h"
#include "querylist.h"
#include "segment7.h"
#include "statemachine.h"
#include "calibrate.h"
#include "menu-roomba.h"
#include "game.h"
#include "brrroomba.h"
#include "settings.h"
#include "chuck_norris.h"

/******************************************************************* Defines */

// We have IR
#define HAVE_IR 1

/******************************************************* Function prototypes */

static uint8_t st_init();
static uint8_t st_menu();
static uint8_t brrroomba_exit();
static uint8_t game_roles();

/************************************************************** Global const */

/********************************************************** Global variables */

/*************************************************************** Local const */

/*********************************************************** Local variables */

/**
 * All possible states for the menu.
 */
enum state_t {
	ST_INIT,
	ST_MENU,
	ST_EXIT
};


static menu_t game = {
	2,
	{
		{
			"Start",
			&game_roles,
			NULL
		},
		{
			"Manual",
			&manual_main,
			NULL
		},
	}
};

static menu_t mainmenu = {
	4,
	{
		{
			"Game",
			NULL,
			&game,
		},
		{
			"Settings",
			NULL,
			&settings_menu,
		},
		{
			"Calibration",
			NULL,
			&calibration_menu,
		},
		{
			"Exit",
			&brrroomba_exit,
			NULL,
		},
	}
};

/******************************************************************** Macros */

/********************************************************** Global functions */

/*********************************************************** Local functions */

int main(int argc, char *argv[]) {

	transition_t transitions[] = {
		// cur-state -> new-state
		{ ST_INIT,	&st_init	},
		{ ST_MENU,	&st_menu	},
	};

	sm_start(
		ST_INIT, ST_EXIT,
		transitions, sizeof(transitions) / sizeof(transition_t*)
	);

	set_7seg_string("Exit");

	return 0;
}

/**
 * Powers down the Roomba. Sends the command ::CMD_POWER.
 * The Board enters the init state after power off. Thus the Roomba can be turned on again,
 * to restart the program.
 */
static uint8_t brrroomba_exit() {
	uart_write_byte(CMD_POWER);
	st_init();
	return MENU_CONTINUE;
}

/**
 * Starts the game depending on the selected role (see ::brrroomba.role).
 */
static uint8_t game_roles() {
	switch(brrroomba.role) {
		case ROLE_USER:
			game_main();
			break;
		case ROLE_CHUCK_NORRIS:
			chuck_norris_main();
			break;
	}

	return MENU_CONTINUE;
}

/******************************************************************** States */

/**
 * State: Initialization.
 */
static uint8_t st_init() {
	init_roomba();

	msleep(100);
	if (BUTTON_PRESSED(1)) {
		return ST_INIT;
	}

	load_defaults();

	return ST_MENU;
}

/**
 * State: Menu
 *
 * Displays the menu defined in ::mainmenu.
 *
 * @return ST_MENU
 */
static uint8_t st_menu() {
	display_menu(&mainmenu, 1, 1);
	return ST_MENU;
}
