package midiconverter;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

public class MIDIConverter {

	public static void main(String[] args) {
		try {
			
			// user input
			Scanner scanner = new Scanner(System.in);
			
			System.out.println("Enter file: ");
			String file = scanner.nextLine();
			
			System.out.println("Enter track number: ");
			int tracknum = scanner.nextInt();
			scanner.close();
			
			Sequence sequence = MidiSystem.getSequence(new File(file));
			Track track = sequence.getTracks()[tracknum];
			
			String[] notes_def = new String[] {
				"C", "C+1", "D", "D+1", "E", "F", "F+1", "G", "G+1", "A", "A+1", "B",
			};
			
			// Parsing
			int num_notes = 0;
			int current_note = 0;
			long current_tick = 0;
			long note_off_tick = 0;
			StringBuffer output = new StringBuffer();
			boolean wait_for_note_off = false;
            for (int i=0; i < track.size(); i++) { 
                MidiEvent event = track.get(i);
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage) {
                    ShortMessage sm = (ShortMessage) message;
                    if (!wait_for_note_off && sm.getCommand() == ShortMessage.NOTE_ON) {
                    	current_tick = event.getTick();
                        current_note = sm.getData1();
                        wait_for_note_off = true;
                        
                        if ((current_tick - note_off_tick) > 2) {
                        	num_notes++;
                        	output.append("{ PAUSE\t\t, " + (current_tick - note_off_tick) + "\t},");
                    		output.append("\n");
                        }
                        
                    } else if (wait_for_note_off && (sm.getCommand() == ShortMessage.NOTE_OFF || 
                    		sm.getCommand() == ShortMessage.NOTE_ON && sm.getData2() == 0)) {
                    	if (sm.getData1() == current_note) {
                            int octave = (current_note / 12)-1;
                            int note = current_note % 12;
                    		wait_for_note_off = false;
                    		note_off_tick = event.getTick();

                    		num_notes++;
                    		output.append("{ NOTE_" + notes_def[note] + "+\t" + octave + "*OCTAVE, " + (note_off_tick - current_tick) + "\t},");
                    		output.append("\n");
                    	}
                    }
                }
            }
            
            String header = "{" + "\n" +
            		"BPM," + "\n" +
            		"BEATS," + "\n" +
            		num_notes + "," + "\n" +
            		"{" + "\n";
            output.insert(0, header);
            output.append("}" + "\n" + "}");

            System.out.println(output);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
